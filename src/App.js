import { withStyles } from '@material-ui/core';
import React, { lazy, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';
import withLayout from './HOCs/HomeLayout';
import { createAction } from './redux/actions/actionCreator';
import { SET_TOKEN_USER } from './redux/actions/type';
import styles from './style';
const Home = lazy(() => import('./pages/Home/Home'));
const Detail = lazy(() => import('./pages/Detail/Detail'));
const Checkout = lazy(() => import('./pages/Checkout/Checkout'));
const Account = lazy(() => import('./pages/Account/Account'));
const HomePage = withLayout(Home, true);
const DetailPage = withLayout(Detail, false);
const CheckoutPage = withLayout(Checkout, false);
const AccountPage = withLayout(Account, false);
const App = function App(props) {
	//init Hook
	const dispatch = useDispatch();
	useEffect(() => {
		const infoUser = JSON.parse(localStorage.getItem('infoUser'));
		if (infoUser) dispatch(createAction(SET_TOKEN_USER, infoUser));
	}, [dispatch]);

	return (
		<BrowserRouter>
			<ScrollToTop />
			<Switch>
				<Route path="/detail/:movieId" component={DetailPage} />
				<Route path="/checkout/:scheduleId" component={CheckoutPage} />
				<Route path="/account/:positionTab" component={AccountPage} />
				<Route path="/" component={HomePage} />
			</Switch>
		</BrowserRouter>
	);
};

export default withStyles(styles)(App);
