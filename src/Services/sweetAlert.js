import Swal from 'sweetalert2';
class SweetAlert {
	alertTimer(title, icon, timer) {
		return Swal.fire({
			title: title,
			icon: icon,
			showConfirmButton: false,
			timer: timer,
		});
	}
	alertChoice(title, btnText, icon) {
		return Swal.fire({
			title: title,
			icon: icon,
			showCancelButton: true,
			confirmButtonColor: '#fdb73b',
			cancelButtonColor: '#d33',
			confirmButtonText: btnText,
			cancelButtonText: 'Hủy',
		});
	}
}
export default SweetAlert;
