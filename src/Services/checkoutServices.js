import connector from '../configs/connector';
class CheckoutService {
	getInfoCheckout(scheduleId) {
		return connector({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=' + scheduleId,
			method: 'GET',
		});
	}
	postBookingRequest(infoBooking) {
		return connector({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/DatVe',
			method: 'POST',
			data: infoBooking,
		});
	}
}

export default CheckoutService;
