import Axios from 'axios';

class TheaterService {
	getListTheaterGroups() {
		return Axios({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinHeThongRap',
			method: 'GET',
		});
	}

	getListTheaterByIdGroup(id) {
		return Axios({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=' + id,
			method: 'GET',
		});
	}

	getListMoviesInTheater(idTheater) {
		return Axios({
			url:
				'https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=' +
				idTheater +
				'&maNhom=GP09',
			method: 'GET',
		});
	}

	getInfoShowMovie(idMovie) {
		return Axios({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=' + idMovie,
			method: 'GET',
		});
	}
}
export default TheaterService;
