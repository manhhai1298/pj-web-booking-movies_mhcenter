import CheckoutService from './checkoutServices';
import MovieService from './movieService';
import SweetAlert from './sweetAlert';
import TheaterService from './theaterService';
import UserService from './userService';

export const movieService = new MovieService();
export const theaterService = new TheaterService();
export const userService = new UserService();
export const checkoutService = new CheckoutService();
export const swAlertService = new SweetAlert();
