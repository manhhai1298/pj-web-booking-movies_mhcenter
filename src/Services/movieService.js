import Axios from 'axios';
import connector from '../configs/connector';

class MovieService {
	getListMovies() {
		return Axios({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09',
			method: 'GET',
		});
	}
	searchMovieByName(movieName) {
		return connector({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP09&tenPhim=' + movieName,
			method: 'GET',
		});
	}
}
export default MovieService;
