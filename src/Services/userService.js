import Axios from 'axios';
import connector from '../configs/connector';

class UserService {
	signIn(user) {
		return Axios({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap',
			method: 'POST',
			data: user,
		});
	}

	signUp(user) {
		return Axios({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy',
			method: 'POST',
			data: user,
		});
	}

	getInfoAccount(account) {
		return connector({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan',
			method: 'POST',
			data: account,
		});
	}

	updateInfoAccount(account) {
		return connector({
			url: 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung',
			method: 'PUT',
			data: account,
		});
	}
}
export default UserService;
