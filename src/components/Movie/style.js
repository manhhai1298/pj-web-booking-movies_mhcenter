const styles = (theme) => {
	return {
		boxInfoMovie: {},
		itemMovie: {
			textAlign: 'center',
			borderRadius: 7,
			margin: '0 8px',
			marginBottom: 50,
			//CARD
			'box-shadow': '4px 5px 6px 4px rgba(0,0,0,0.2)',
			transition: '0.3s',
			'&:hover': {
				'box-shadow': '4.5px 5.5px 6.5px 4.5px rgba(0,0,0,0.25)',
				transition: '0.3s',
				'& $boxCoverColor': {
					borderRadius: '7px 7px 0 0',
					transition: 'all 0.5s ease-out',
					opacity: 1,
				},
				'& $btnPlayTrailer': {
					cursor: 'pointer',
					opacity: 1,
					visibility: 'visible',
					top: '50%',
					transition: 'all 0.35s ease-out',
				},
				'& $btnBooking': {
					display: 'block',
				},
				'& $boxInfoMovie': {
					display: 'none',
				},
			},
		},
		imgMovie: {
			borderRadius: '7px 7px 0 0',
			width: '100%',
			height: '19.5vw',
		},
		btnPlayTrailer: {
			color: '#fff',
			fontSize: 60,
			position: 'absolute',
			top: '35%',
			left: '50%',
			transform: 'translate(-50%,-50%)',
			zIndex: 11,
			opacity: 0,
			visibility: 'hidden',
			transition: 'all 0.35s ease-in',
		},
		boxCoverColor: {
			borderRadius: 7,
			position: 'absolute',
			left: 0,
			top: 0,
			right: 0,
			bottom: 0,
			background: 'rgba(0, 0, 0, 0.45)',
			transition: 'all 0.5s ease-in',
			opacity: 0,
		},
		titleMovie: {
			fontSize: 18,
			fontWeight: '600',
			marginTop: 2,
			height: 50,
			display: '-webkit-box',
			'-webkit-line-clamp': 2,
			'-webkit-box-orient': 'vertical',
			overflow: 'hidden',
		},
		descMovie: {
			fontSize: 15,
			marginBottom: 1,
			display: '-webkit-box',
			'-webkit-line-clamp': 1,
			'-webkit-box-orient': 'vertical',
			overflow: 'hidden',
		},
		styleAlertTrailer: {
			padding: theme.spacing(1),
			width: 'auto',
			height: 'auto',
			'& .swal2-content': {
				padding: 0,
			},
		},
		btnBooking: {
			margin: '6px 0',
			height: 40,
			fontSize: 18,
			fontWeight: 600,
			display: 'none',
		},
		[theme.breakpoints.down('xl')]: {
			imgMovie: {
				height: '17vw',
			},
			descMovie: {
				display: '-webkit-box',
				'-webkit-line-clamp': 1,
				'-webkit-box-orient': 'vertical',
				overflow: 'hidden',
			},
		},
		[theme.breakpoints.down('lg')]: {
			imgMovie: {
				height: '23vw',
			},
			descMovie: {
				display: '-webkit-box',
				'-webkit-line-clamp': 1,
				'-webkit-box-orient': 'vertical',
				overflow: 'hidden',
			},
		},
		[theme.breakpoints.down('md')]: {
			imgMovie: {
				height: '30.5vw',
			},
			descMovie: {
				display: '-webkit-box',
				'-webkit-line-clamp': 1,
				'-webkit-box-orient': 'vertical',
				overflow: 'hidden',
			},
		},
		[theme.breakpoints.down('sm')]: {
			imgMovie: {
				height: '39vw',
			},
			descMovie: {
				display: '-webkit-box',
				'-webkit-line-clamp': 1,
				'-webkit-box-orient': 'vertical',
				overflow: 'hidden',
			},
		},
		[theme.breakpoints.down('xs')]: {
			imgMovie: {
				height: '58vw',
			},
			styleRating: {
				fontSize: 18,
			},
			descMovie: {
				display: '-webkit-box',
				'-webkit-line-clamp': 1,
				'-webkit-box-orient': 'vertical',
				overflow: 'hidden',
			},
		},
	};
};
export default styles;
