import { Box, Button, Typography, withStyles } from '@material-ui/core';
import { Rating } from '@material-ui/lab';
import PlayCircleOutlineRoundedIcon from '@material-ui/icons/PlayCircleOutlineRounded';
import React from 'react';
import styles from './style';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import LazyLoad from 'react-lazyload';

const Movie = ({ classes, movie, colorText, imgMovie }) => {
	const history = useHistory();

	const handleWidthTrailer = () => {
		const widthScreen = window.innerWidth;
		if (widthScreen <= 600) return `width="280" height="220"`;
		else if (widthScreen > 600 && widthScreen <= 960) return `width="540" height="380"`;
		else if (widthScreen > 960 && widthScreen <= 1280) return `width="864" height="480"`;
		else if (widthScreen > 1280 && widthScreen <= 1920) return `width="1024" height="576"`;
		else if (widthScreen > 1920) return `width="1366" height="768"`;
		return `width="854" height="480"`;
	};
	//View Trailer Movie
	const handleViewTrailer = (url) => {
		Swal.fire({
			showCancelButton: true,
			cancelButtonText: 'Đóng',
			cancelButtonColor: '#d33',
			showConfirmButton: false,
			customClass: classes.styleAlertTrailer,
			html: `<iframe ${handleWidthTrailer()} src="${url}?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`,
		});
	};

	const handleBooking = () => {
		//Get Token user signin from redux
		history.push({ pathname: `/detail/${movie.maPhim}` });
	};

	return (
		<div className={classes.itemMovie}>
			<Box style={{ position: 'relative' }}>
				<LazyLoad once={true}>
					<img
						src={movie.hinhAnh.indexOf('https') === -1 ? movie.hinhAnh.replace('http', 'https') : null}
						alt=""
						className={classes.imgMovie}
						style={{
							height: `${imgMovie.height}`,
							width: `${imgMovie.width}`,
							borderRadius: `${imgMovie.borderRadius}`,
						}}
					/>
				</LazyLoad>
				<PlayCircleOutlineRoundedIcon
					className={classes.btnPlayTrailer}
					onClick={() => handleViewTrailer(movie.trailer)}
				/>
				<Box className={classes.boxCoverColor}></Box>
			</Box>
			<Typography className={`${classes.titleMovie} ${classes.boxInfoMovie}`} style={{ color: `${colorText}` }}>
				{movie.tenPhim}
			</Typography>

			<Button
				variant="contained"
				color="primary"
				className={classes.btnBooking}
				fullWidth={true}
				onClick={handleBooking}
			>
				Mua vé
			</Button>

			<Typography className={classes.descMovie} style={{ color: `${colorText}` }}>
				{movie.ngayKhoiChieu.slice(0, 4)} - {movie.typeMovie}
			</Typography>
			<Rating
				name="hover-feedback"
				value={movie.danhGia}
				precision={0.5}
				readOnly
				className={classes.styleRating}
			/>
		</div>
	);
};

Movie.defaultProps = { imgMovie: 'default' };
export default withStyles(styles)(React.memo(Movie));
