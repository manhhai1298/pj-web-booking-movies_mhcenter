import React, { useState } from 'react';
import styles from './style';
import { Box, Button, IconButton, Modal, TextField, Typography, withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import SearchIcon from '@material-ui/icons/Search';
import PermIdentityRoundedIcon from '@material-ui/icons/PermIdentityRounded';
import MenuRoundedIcon from '@material-ui/icons/MenuRounded';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';
import Logo from '../../assets/image/logo/logo_full.svg';
import LogoResponsive from '../../assets/image/logo/logo_full_responsive.svg';
import Signin from '../SignIn/Signin';
import { useDispatch, useSelector } from 'react-redux';
import { SET_TOKEN_USER } from '../../redux/actions/type';
import { createAction } from '../../redux/actions/actionCreator';
import { movieService, swAlertService } from '../../Services/initServices';
import Signup from '../SignUp/Signup';
import { Link, useHistory } from 'react-router-dom';
import { Link as LinkScroll } from 'react-scroll';
import { Link as LinkRoute } from 'react-router-dom';
import { Form, Formik } from 'formik';
import LazyLoad from 'react-lazyload';

const Header = ({ classes, typeMenu }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [isShowMenuToggle, setIsShowMenuToggle] = useState(false);
	const [isOpenSignIn, setIsOpenSignIn] = useState(false);
	const [isOpenSignUp, setIsOpenSignUp] = useState(false);
	const handleBtnToggle = () => {
		setIsShowMenuToggle(!isShowMenuToggle);
	};
	const handleSignOut = () => {
		swAlertService.alertChoice('Bạn muốn đăng xuất ?', 'Đăng xuất', 'question').then((result) => {
			if (result.value) {
				localStorage.setItem('infoUser', JSON.stringify({ accessToken: '', hoTen: '' }));
				dispatch(createAction(SET_TOKEN_USER, { accessToken: '', hoTen: '' }));
				history.push({ pathname: `/` });
				swAlertService.alertTimer('Đăng xuất thành công !', 'success', 2000);
			}
		});
	};
	//Get Token user signin from redux
	const userSignIn = useSelector((state) => state.user.rdInfoUser);

	//Formik
	const handleSearchMovie = (value) => {
		if (value.tenPhim) {
			const nameMovie = value.tenPhim.replace(' ', '%20').replace(':', '%3A');
			movieService
				.searchMovieByName(nameMovie)
				.then((res) => history.push({ pathname: `/detail/${res.data[0].maPhim}` }))
				.catch((err) => swAlertService.alertTimer('Không có phim bạn cần tìm !', 'error', 2000));
		} else swAlertService.alertTimer('Tên phim không hợp lệ !', 'warning', 2000);
	};
	return (
		<div className={classes.styleDivHeader}>
			<Grid container style={{ position: 'relative' }}>
				<Grid item xs={11} sm={10} md={6}>
					<Box display="flex" alignItems="center">
						<Link to={`/`}>
							<LazyLoad once={true}>
								<img src={Logo} alt="logo_mhcenter_bookingmovietickets" className={classes.styleLogo} />
							</LazyLoad>
							<LazyLoad once={true}>
								<img
									src={LogoResponsive}
									alt="logo_mhcenter_bookingmovietickets"
									className={classes.styleLogoResponsive}
								/>
							</LazyLoad>
						</Link>
						<Formik initialValues={{ tenPhim: '' }} onSubmit={handleSearchMovie}>
							{(formikProps) => (
								<Form style={{ display: 'flex' }}>
									<TextField
										InputProps={{
											className: classes.styleTextField,
											disableUnderline: true,
										}}
										placeholder="Tìm kiếm phim..."
										name="tenPhim"
										onChange={formikProps.handleChange}
									></TextField>
									<Button classes={{ root: classes.styleBtnSearch }} type="submit">
										<SearchIcon />
									</Button>
								</Form>
							)}
						</Formik>
					</Box>
				</Grid>
				<Grid item xs={1} sm={2} md={6}>
					{/* Box right header */}
					<Box display="flex" alignItems="center" justifyContent="flex-end" height="100%">
						{/* Box menu */}
						<Box className={classes.boxMenu}>
							<ul className={classes.styleMenu}>
								<Link
									to={`/`}
									style={{ textDecoration: 'none', color: 'black' }}
									classes={{ root: classes.styleLinkActive }}
								>
									<li>
										<Typography>Trang Chủ</Typography>
									</li>
								</Link>
								{typeMenu ? (
									<>
										<LinkScroll
											className={classes.linkMenu}
											to="ListMovies"
											spy={true}
											smooth={true}
											duration={1000}
										>
											<li>
												<Typography>Phim</Typography>
											</li>
										</LinkScroll>
										<LinkScroll
											className={classes.linkMenu}
											to="ListTheaters"
											spy={true}
											smooth={true}
											duration={1000}
										>
											<li>
												<Typography>Cụm Rạp</Typography>
											</li>
										</LinkScroll>
									</>
								) : (
									''
								)}
								<LinkScroll
									className={classes.linkMenu}
									to="Footer"
									spy={true}
									smooth={true}
									duration={1000}
								>
									<li>
										<Typography>Liên hệ</Typography>
									</li>
								</LinkScroll>
							</ul>
						</Box>
						{/* Box menu */}

						{/* Toggle menu responsive */}
						<Box className={classes.toggleMenu}>
							<IconButton
								classes={{ root: classes.styleBtnAccount }}
								disableRipple={true}
								onClick={handleBtnToggle}
							>
								{isShowMenuToggle ? <CloseRoundedIcon /> : <MenuRoundedIcon />}
							</IconButton>
						</Box>

						{/* Toggle menu responsive */}

						{/* Box button account */}
						<Box className={classes.styleBoxAccount}>
							{/* Button Account */}
							<IconButton classes={{ root: classes.styleBtnAccount }} disableRipple={true}>
								<PermIdentityRoundedIcon className={classes.iconAccount} />
								{userSignIn.accessToken && userSignIn.hoTen ? (
									<Typography>
										{userSignIn.hoTen.indexOf(' ') !== -1
											? userSignIn.hoTen.split(' ').slice(1, 3).join(' ')
											: userSignIn.hoTen}
									</Typography>
								) : (
									<Typography>Tài khoản</Typography>
								)}
							</IconButton>
							{/* Button Account */}
							<Box classes={{ root: classes.animationBoxExpand }}>
								{/* arrow up */}
								<Box
									style={{
										width: 0,
										height: 0,
										borderLeft: '10px solid transparent',
										borderRight: '10px solid transparent',
										borderBottom: '10px solid #fff',
										position: 'absolute',
										left: '45%',
										bottom: 0,
									}}
								></Box>
								{/* arrow up */}

								{/* space expand when hover button account */}
								<Box classes={{ root: classes.styleBoxExpand }}>
									{userSignIn.accessToken ? (
										<Box>
											<Box m={1}>
												<LinkRoute
													to={`/account/0`}
													style={{ textDecoration: 'none', color: 'black' }}
												>
													<Button variant="contained" color="primary" fullWidth={true}>
														Tài Khoản
													</Button>
												</LinkRoute>
											</Box>
											<Box m={1}>
												<LinkRoute
													to={`/account/1`}
													style={{ textDecoration: 'none', color: 'black' }}
												>
													<Button variant="contained" color="primary" fullWidth={true}>
														Đổi mật khẩu
													</Button>
												</LinkRoute>
											</Box>
											<Box m={1}>
												<Button
													variant="contained"
													color="primary"
													fullWidth={true}
													onClick={handleSignOut}
												>
													Đăng Xuất
												</Button>
											</Box>
										</Box>
									) : (
										<Box>
											<Box m={1}>
												<Button
													variant="contained"
													color="primary"
													fullWidth={true}
													onClick={() => setIsOpenSignIn(true)}
												>
													Đăng Nhập
												</Button>
											</Box>
											<Box m={1}>
												<Button
													variant="contained"
													color="primary"
													fullWidth={true}
													onClick={() => setIsOpenSignUp(true)}
												>
													Đăng Ký
												</Button>
											</Box>
										</Box>
									)}
								</Box>
								{/* space expand when hover button account */}
							</Box>
						</Box>
						{/* Box button account */}
					</Box>
					{/* Box right header */}
				</Grid>
			</Grid>

			{/* Box menu responsive */}
			<Box classes={{ root: `${isShowMenuToggle ? classes.boxMenuToggleShow : classes.boxMenuToggle}` }}>
				<ul className={`${isShowMenuToggle ? classes.styleMenuToggleShow : classes.styleMenuToggle}`}>
					<Link to={`/`} color="inherit" href="#" style={{ textDecoration: 'none', color: '#fdb73b' }}>
						<li>
							<Typography>Trang Chủ</Typography>
						</li>
					</Link>

					{typeMenu === true ? (
						<>
							<LinkScroll
								to="ListMovies"
								spy={true}
								smooth={true}
								duration={1000}
								color="inherit"
								href="#"
								style={{ textDecoration: 'none', color: '#fdb73b' }}
							>
								<li>
									<Typography>Phim</Typography>
								</li>
							</LinkScroll>
							<LinkScroll
								to="ListTheaters"
								spy={true}
								smooth={true}
								duration={1000}
								color="inherit"
								href="#"
								style={{ textDecoration: 'none', color: '#fdb73b' }}
							>
								<li>
									<Typography>Cụm Rạp</Typography>
								</li>
							</LinkScroll>
						</>
					) : (
						''
					)}
					<LinkScroll
						to="Footer"
						spy={true}
						smooth={true}
						duration={700}
						color="inherit"
						href="#"
						style={{ textDecoration: 'none', color: '#fdb73b' }}
					>
						<li>
							<Typography>Liên hệ</Typography>
						</li>
					</LinkScroll>
					{userSignIn.accessToken ? (
						<Box>
							<Link
								to={`/account/0`}
								color="inherit"
								href="#"
								style={{ textDecoration: 'none', color: '#fdb73b' }}
							>
								<li>
									<Typography>Tài khoản</Typography>
								</li>
							</Link>
							<Link
								to={`/account/1`}
								color="inherit"
								href="#"
								style={{ textDecoration: 'none', color: '#fdb73b' }}
							>
								<li>
									<Typography>Đổi mật khẩu</Typography>
								</li>
							</Link>
							<li onClick={handleSignOut}>
								<Typography>Đăng xuất</Typography>
							</li>
						</Box>
					) : (
						<Box>
							<li onClick={() => setIsOpenSignIn(true)}>
								<Typography>Đăng nhập</Typography>
							</li>
							<li onClick={() => setIsOpenSignUp(true)}>
								<Typography>Đăng ký</Typography>
							</li>
						</Box>
					)}
				</ul>
			</Box>

			<Modal
				open={isOpenSignIn}
				onClose={() => setIsOpenSignIn(false)}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
				disableAutoFocus={true}
			>
				<Signin
					closeSignInModal={() => setIsOpenSignIn(false)}
					btnSignUpBonus={() => {
						setIsOpenSignIn(false);
						setIsOpenSignUp(true);
					}}
				/>
			</Modal>

			<Modal
				open={isOpenSignUp}
				onClose={() => setIsOpenSignUp(false)}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
				disableAutoFocus={true}
			>
				<Signup closeSignUpModal={() => setIsOpenSignUp(false)} />
			</Modal>
		</div>
	);
};
export default withStyles(styles)(React.memo(Header));
