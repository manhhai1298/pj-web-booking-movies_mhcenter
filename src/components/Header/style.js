const styles = (theme) => {
	return {
		styleDivHeader: {
			backgroundColor: theme.palette.primary.main,
			padding: theme.spacing(0, 6),
			height: '100%',
			position: 'relative',
		},
		styleLogoResponsive: {
			display: 'none',
		},
		styleTextField: {
			height: 40,
			width: '25vw',
			backgroundColor: '#fff',
			borderRadius: '20px 0 0 20px',
			padding: theme.spacing(0, 2),
			marginLeft: theme.spacing(3),
		},
		styleBtnSearch: {
			backgroundColor: '#fff',
			height: 40,
			borderRadius: '0 20px 20px 0',
			'&:hover': {
				backgroundColor: '#fff',
			},
		},
		styleMenu: {
			display: 'flex',
			listStyleType: 'none',
			'& li': {
				marginRight: theme.spacing(3),
				position: 'relative',
				'&::after': {
					content: `''`,
					display: 'block',
					height: 1.5,
					background: 'black',
					position: 'absolute',
					bottom: -3,
					left: 0,
					width: 0,
					transition: 'all 0.2s ease-in-out',
				},
				'&:hover::after': {
					width: '100%',
					transition: 'all 0.2s ease-in-out',
				},
			},
		},
		styleLinkActive: {
			'& li': {
				position: 'relative',
				'&::after': {
					content: `''`,
					display: 'block',
					height: 1.5,
					background: 'black',
					position: 'absolute',
					bottom: -3,
					left: 0,
					width: '100%',
				},
			},
		},
		styleBoxAccount: {
			borderLeft: '1px solid black',
			position: 'relative',
		},
		styleBtnAccount: {
			color: 'black',
			'&:hover': {
				backgroundColor: theme.palette.primary.main,
			},
			'&:hover + $animationBoxExpand': {
				visibility: 'visible',
				transition: 'all 0.2s ease-in-out',
			},
			'&::focus': {
				backgroundColor: theme.palette.primary.main,
			},
		},
		styleBoxExpand: {
			zIndex: 11,
			position: 'absolute',
			backgroundColor: '#fff',
			width: '10.5vw',
			left: '55%',
			transform: 'translate(-50%, 0)',
			borderRadius: 4,
			boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
		},
		animationBoxExpand: {
			visibility: 'hidden',
			transition: 'all 0.2s ease-in-out',
			'&:hover': {
				visibility: 'visible',
				transition: 'all 0.2s ease-in-out',
			},
		},
		toggleMenu: {
			display: 'none',
		},
		boxMenuToggle: {
			zIndex: 15,
			textAlign: 'center',
			display: 'flex',
			position: 'absolute',
			backgroundColor: '#000000eb',
			color: theme.palette.primary.main,
			width: '100%',
			// center absolute & relative
			left: '50%',
			transform: 'translate(-50%, 0)',
			// center absolute & relative
			height: '0',
			transition: 'all 0.3s ease-in-out',
		},
		boxMenuToggleShow: {
			zIndex: 15,
			textAlign: 'center',
			display: 'flex',
			position: 'absolute',
			backgroundColor: '#000000eb',
			color: theme.palette.primary.main,
			width: '100%',
			// center absolute & relative
			left: '50%',
			transform: 'translate(-50%, 0)',
			// center absolute & relative
			height: 380,
			transition: 'all 0.3s ease-in-out',
		},
		styleMenuToggle: {
			opacity: 0,
			visibility: 'hidden',
			listStyle: 'none',
			padding: 0,
			margin: 0,
			width: '100%',
			'& li': {
				borderTop: '1px solid #fdb73b',
				padding: theme.spacing(1.5, 0),
			},
			transition: 'all 0.3s ease-out',
		},
		styleMenuToggleShow: {
			opacity: 1,
			visibility: 'visible',
			listStyle: 'none',
			padding: 0,
			margin: 0,
			width: '100%',
			'& li': {
				borderTop: '1px solid #fdb73b',
				padding: theme.spacing(1.5, 0),
			},
			transition: 'all 0.45s ease-in',
		},
		linkMenu: {
			textDecoration: 'none',
			color: 'black',
			'&:hover': { cursor: 'pointer' },
		},
		[theme.breakpoints.down('md')]: {
			styleMenu: {
				'& li': {
					marginRight: '1vw',
				},
			},
			styleBoxAccount: {
				position: 'relative',
				marginLeft: '0px',
			},
			styleTextField: {
				width: '19vw',
			},
			styleBoxExpand: {
				width: '14vw',
			},
		},
		[theme.breakpoints.down('sm')]: {
			styleDivHeader: {
				padding: theme.spacing(0, 3),
			},
			boxMenu: {
				display: 'none',
			},
			toggleMenu: {
				border: '2px solid black',
				borderRadius: 5,
				display: 'block',
			},
			styleBoxAccount: {
				display: 'none',
			},
		},
		[theme.breakpoints.down('xs')]: {
			styleDivHeader: {
				padding: theme.spacing(0, 1),
			},
			styleTextField: {
				height: 40,
				width: '25vw',
				backgroundColor: '#fff',
				borderRadius: '20px 0 0 20px',
				padding: theme.spacing(0, 2),
				marginLeft: theme.spacing(1),
			},
			styleLogo: {
				display: 'none',
			},
			styleLogoResponsive: {
				display: 'block',
			},
		},
	};
};

export default styles;
