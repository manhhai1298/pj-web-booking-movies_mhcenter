const styles = () => {
	return {
		titleTheater: { transition: 'all 0.4s' },
		theaterAddress: { transition: 'all 0.4s' },
		rowTheaterSelected: {
			opacity: 1,
			transition: 'all 0.4s',
		},
		rowTheater: {
			opacity: 0.45,
			transition: 'all 0.4s',
			'&:hover': {
				cursor: 'pointer',
				opacity: 1,
				transition: 'all 0.4s',
				fontSize: 26,
			},
			'&:hover $titleTheater': { fontSize: 23, transition: 'all 0.4s' },
			'&:hover $theaterAddress': { fontSize: 14, transition: 'all 0.4ss' },
		},
	};
};
export default styles;
