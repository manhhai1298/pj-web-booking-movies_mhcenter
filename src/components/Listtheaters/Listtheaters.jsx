import { Box, Table, TableBody, TableCell, TableRow, Typography, withStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createAction } from '../../redux/actions/actionCreator';
import { theaterService } from '../../Services/initServices';
import { GET_LIST_THEATER_BY_ID } from '../../redux/actions/type';
import { POST_ID_THEATER_CHOOSE } from '../../redux/actions/type';
import styles from './style';
import LoadFetch from '../CompLoading/LoadComponent/LoadFetch';
const Listtheaters = ({ classes }) => {
	const dispatch = useDispatch();
	const [isRowSelected, setIsRowSelected] = useState(0);
	const { maHeThongRap, logo } = useSelector((state) => state.theater.theaterGroupChoosed);

	//CALL API
	useEffect(() => {
		theaterService
			.getListTheaterByIdGroup(maHeThongRap)
			.then((res) => {
				dispatch(createAction(GET_LIST_THEATER_BY_ID, res.data));
			})
			.catch();
	}, [dispatch, maHeThongRap]);

	const listTheater = useSelector((state) => state.theater.listTheatersByIdGroup);

	const chooseTheater = (idTheaterChoosed, index) => {
		setIsRowSelected(index);
		dispatch(createAction(POST_ID_THEATER_CHOOSE, idTheaterChoosed));
	};

	return listTheater.length > 0 ? (
		<div style={{ width: '100%', marginRight: 20 }}>
			<Box style={{ height: 80 }} display="flex" alignItems="center" justifyContent="center">
				<Typography variant="h4" contained="h4">
					CHỌN RẠP
				</Typography>
			</Box>
			<Box style={{ maxHeight: 600, overflow: 'auto' }}>
				<Table>
					<TableBody>
						{listTheater.map((theater, index) => (
							<TableRow
								key={index}
								onClick={() => (isRowSelected === index ? '' : chooseTheater(theater.maCumRap, index))}
								className={isRowSelected === index ? classes.rowTheaterSelected : classes.rowTheater}
							>
								<TableCell>
									<Box style={{ display: 'flex' }}>
										<img
											src={logo.indexOf('https') === -1 ? logo.replace('http', 'https') : null}
											alt="theater-img"
											style={{ width: '60px', height: '60px', marginRight: 10 }}
										/>
										<Box>
											<Typography
												variant="h5"
												contained="h4"
												color="primary"
												className={classes.titleTheater}
											>
												{theater.tenCumRap}
											</Typography>
											<Typography className={classes.theaterAddress}>{theater.diaChi}</Typography>
										</Box>
									</Box>
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</Box>
		</div>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(Listtheaters));
