const styles = (theme) => {
	return {
		boxCellMovie: { display: 'flex', marginBottom: 30 },
		[theme.breakpoints.down('xs')]: {
			boxCellMovie: { display: 'block' },
			boxInfoMovie: { marginTop: 15 },
			boxChoiceSchedule: { marginTop: 20 },
		},
	};
};
export default styles;
