import { Box, Button, Modal, Table, TableBody, TableCell, TableRow, Typography, withStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { theaterService } from '../../Services/initServices';
import { createAction } from '../../redux/actions/actionCreator';
import { GET_LIST_MOVIES_IN_THEATER } from '../../redux/actions/type';
import { useHistory } from 'react-router-dom';
import styles from './style';
import LoadFetch from '../CompLoading/LoadComponent/LoadFetch';
import Signin from '../SignIn/Signin';
import Signup from '../SignUp/Signup';
const ListMoviesTheater = ({ classes }) => {
	const [isOpenSignIn, setIsOpenSignIn] = useState(false);
	const [isOpenSignUp, setIsOpenSignUp] = useState(false);
	const dispatch = useDispatch();
	const history = useHistory();
	const idTheaterChoose = useSelector((state) => state.theater.idTheaterChoose);

	const { maHeThongRap } = useSelector((state) => state.theater.theaterGroupChoosed);

	//Get Token user signin from redux
	const userSignIn = useSelector((state) => state.user.rdInfoUser);

	const handleChoiceSchedule = (scheduleId) => {
		if (userSignIn.accessToken) history.push({ pathname: `/checkout/${scheduleId}` });
		else setIsOpenSignIn(true);
	};

	useEffect(() => {
		theaterService
			.getListMoviesInTheater(maHeThongRap)
			.then((res) => {
				dispatch(createAction(GET_LIST_MOVIES_IN_THEATER, res.data));
			})
			.catch();
	}, [dispatch, maHeThongRap]);

	const listMoviesTheater = useSelector((state) => state.theater.listMoviesInTheater);
	return listMoviesTheater.length > 0 ? (
		<div style={{ width: '100%' }}>
			<div className={classes.boxChoiceSchedule}>
				<Box style={{ height: 80 }} display="flex" alignItems="center" justifyContent="center">
					<Typography variant="h4" containerd="h4">
						CHỌN LỊCH CHIẾU
					</Typography>
				</Box>
				<Box style={{ height: 600, overflow: 'auto' }}>
					<Table>
						<TableBody>
							{listMoviesTheater[0].lstCumRap
								.filter((tile) => tile.maCumRap === idTheaterChoose)
								.map((tile) =>
									tile.danhSachPhim.map((movie, index) => (
										<TableRow key={index}>
											<TableCell>
												<Box className={classes.boxCellMovie}>
													<Box
														style={{
															width: '168px',
															height: 'auto',
															marginRight: 20,
															borderRadius: 7,
														}}
													>
														<img
															src={
																movie.hinhAnh.indexOf('https') === -1
																	? movie.hinhAnh.replace('http', 'https')
																	: null
															}
															alt="img-Movie"
															style={{
																width: '168px',
																height: '230px',
																borderRadius: 7,
															}}
														/>
														<Box
															color="primary"
															style={{
																border: '2px solid #fdb73b',
																borderRadius: 4,
																marginTop: 5,
																fontSize: 12,
																fontWeight: 'bold',
																padding: '0 5px',
																display: 'inline-block',
															}}
														>
															<Typography
																variant="h6"
																color="primary"
																style={{ fontSize: 16 }}
															>
																2D Digital
															</Typography>
														</Box>
													</Box>
													<Box className={classes.boxInfoMovie}>
														<Typography variant="h4">{movie.tenPhim}</Typography>
														<Typography style={{ fontSize: 18, marginTop: 15 }}>
															Lịch chiếu:
														</Typography>

														{movie.lstLichChieuTheoPhim.map((lichChieu) => (
															<Button
																variant="outlined"
																key={lichChieu.maLichChieu}
																style={{
																	margin: 5,
																	backgroundColor: '#fdb73b',
																	width: 150,
																}}
																onClick={() =>
																	handleChoiceSchedule(lichChieu.maLichChieu)
																}
															>
																<Typography style={{ fontSize: 15, fontWeight: '500' }}>
																	{lichChieu.ngayChieuGioChieu.substring(11, 16)}
																</Typography>
																<Typography
																	style={{
																		fontSize: 15,
																		fontWeight: '500',
																		margin: '0 5px',
																	}}
																>
																	~
																</Typography>
																<Typography
																	style={{ fontSize: 20, fontWeight: 'bold' }}
																>
																	{lichChieu.ngayChieuGioChieu.substring(8, 10) +
																		'/' +
																		lichChieu.ngayChieuGioChieu.substring(5, 7)}
																</Typography>
															</Button>
														))}
													</Box>
												</Box>
											</TableCell>
										</TableRow>
									))
								)}
						</TableBody>
					</Table>
				</Box>
			</div>
			<Modal
				open={isOpenSignIn}
				onClose={() => setIsOpenSignIn(false)}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
				disableAutoFocus={true}
			>
				<Signin
					closeSignInModal={() => setIsOpenSignIn(false)}
					btnSignUpBonus={() => {
						setIsOpenSignIn(false);
						setIsOpenSignUp(true);
					}}
				/>
			</Modal>
			<Modal
				open={isOpenSignUp}
				onClose={() => setIsOpenSignUp(false)}
				aria-labelledby="simple-modal-title"
				aria-describedby="simple-modal-description"
				style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
				disableAutoFocus={true}
			>
				<Signup closeSignUpModal={() => setIsOpenSignUp(false)} />
			</Modal>
		</div>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(ListMoviesTheater));
