import { Box, Button, Grid, TextField, Typography, withStyles } from '@material-ui/core';
import { ErrorMessage, Form, Formik } from 'formik';
import React from 'react';
import { swAlertService, userService } from '../../../Services/initServices';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import { createAction } from '../../../redux/actions/actionCreator';
import { SET_TOKEN_USER } from '../../../redux/actions/type';
import styles from './style';
import LoadFetch from '../../CompLoading/LoadComponent/LoadFetch';
const Changepassword = ({ classes, infoAccount }) => {
	const dispatch = useDispatch();

	const signUpObjectValidate = yup.object().shape({
		matKhauCu: yup.string().required('* Bắt buộc nhập'),
		matKhau: yup.string().required('* Bắt buộc nhập').min(3, '* Ít nhất 3 ký tự'),
		nhapMatKhauMoi: yup.string().required('* Bắt buộc nhập'),
	});
	const handleSubmitSignUp = (value) => {
		if (value.matKhauCu !== infoAccount.matKhau) swAlertService.alertTimer('Sai mật khẩu cũ !', 'error', 2000);
		else if (value.matKhau !== value.nhapMatKhauMoi)
			swAlertService.alertTimer('Nhập lại mật khẩu không khớp !', 'error', 2000);
		else {
			const input = {
				taiKhoan: infoAccount.taiKhoan,
				matKhau: value.matKhau,
				email: infoAccount.email,
				soDt: infoAccount.soDT,
				maNhom: 'GP09',
				maLoaiNguoiDung: 'KhachHang',
				hoTen: infoAccount.hoTen,
			};
			userService
				.updateInfoAccount(input)
				.then((res) => {
					const accessToken = JSON.parse(localStorage.getItem('infoUser')).accessToken;

					localStorage.setItem(
						'infoUser',
						JSON.stringify({
							accessToken: accessToken,
							taiKhoan: res.data.taiKhoan,
							hoTen: res.data.hoTen,
						})
					);
					dispatch(
						createAction(SET_TOKEN_USER, {
							accessToken: accessToken,
							taiKhoan: res.data.taiKhoan,
							hoTen: res.data.hoTen,
						})
					);

					swAlertService.alertTimer('Đổi mật khẩu thành công !', 'success', 2000);
					window.location.reload(false);
				})
				.catch((err) => {
					swAlertService.alertTimer(err, 'error', 2000);
				});
		}
	};

	return infoAccount !== undefined ? (
		<div>
			<Box display="flex" justifyContent="center">
				<Typography variant="h5" style={{ color: '#ffa300', marginBottom: 20 }}>
					ĐỔI MẬT KHẨU
				</Typography>
			</Box>
			<Formik
				enableReinitialize
				initialValues={{
					taiKhoan: infoAccount.taiKhoan,
					matKhau: '',
					hoTen: infoAccount.hoTen,
					email: infoAccount.email,
					soDt: infoAccount.soDT,
					maLoaiNguoiDung: 'KhachHang',
					maNhom: 'GP09',
					matKhauCu: '',
					nhapMatKhauMoi: '',
				}}
				onSubmit={handleSubmitSignUp}
				validationSchema={signUpObjectValidate}
			>
				{(formikProps) => (
					<Form style={{ display: 'flex' }}>
						<Box style={{ margin: '0 auto' }}>
							<Grid container spacing={1} alignItems="center" style={{ marginBottom: 10 }}>
								<Grid item>
									<TextField
										label="Mật khẩu cũ"
										type="password"
										className={classes.styleTextField}
										name="matKhauCu"
										onChange={formikProps.handleChange}
										variant="outlined"
									/>
								</Grid>
								<Box display="block">
									<ErrorMessage name="matKhauCu" component="div" style={{ color: 'red' }} />
								</Box>
							</Grid>
							<Grid container spacing={1} alignItems="center" style={{ marginBottom: 10 }}>
								<Grid item>
									<TextField
										label="Mật khẩu mới"
										type="password"
										className={classes.styleTextField}
										name="matKhau"
										onChange={formikProps.handleChange}
										variant="outlined"
									/>
								</Grid>
								<Box display="block">
									<ErrorMessage name="matKhau" component="div" style={{ color: 'red' }} />
								</Box>
							</Grid>
							<Grid container spacing={1} alignItems="center" style={{ marginBottom: 10 }}>
								<Grid item>
									<TextField
										label="Nhập lại mật khẩu mới"
										type="password"
										className={classes.styleTextField}
										name="nhapMatKhauMoi"
										onChange={formikProps.handleChange}
										variant="outlined"
									/>
								</Grid>
								<Box display="block">
									<ErrorMessage name="nhapMatKhauMoi" component="div" style={{ color: 'red' }} />
								</Box>
							</Grid>
							<Grid
								container
								spacing={1}
								alignItems="flex-end"
								justify="center"
								style={{ marginBottom: 10 }}
							>
								<Grid item>
									<Button
										type="submit"
										variant="contained"
										color="primary"
										style={{ margin: '15px 0 10px 0' }}
										// disabled={isDisableBtn}
									>
										Đổi mật khẩu
									</Button>
								</Grid>
							</Grid>
						</Box>
					</Form>
				)}
			</Formik>
		</div>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(Changepassword));
