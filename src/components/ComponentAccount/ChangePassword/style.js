const styles = (theme) => {
	return {
		styleTextField: {
			width: '30vw',
		},
		[theme.breakpoints.down('sm')]: {
			styleTextField: {
				width: '45vw',
			},
		},
		[theme.breakpoints.down('xs')]: {
			styleTextField: {
				width: '65vw',
			},
		},
	};
};
export default styles;
