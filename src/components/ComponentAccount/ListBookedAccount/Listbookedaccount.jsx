import {
	Box,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TablePagination,
	TableRow,
	Typography,
	withStyles,
} from '@material-ui/core';
import React from 'react';
import LoadFetch from '../../CompLoading/LoadComponent/LoadFetch';
import styles from './style';
const Listbookedaccount = ({ classes, infoAccount }) => {
	let listBooking = [];
	let listSeats = '';
	let total = 0;
	let objectTemp = null;
	if (infoAccount) {
		const handleListBooking = () => {
			infoAccount.thongTinDatVe.filter((thongtin) => {
				thongtin.danhSachGhe.filter((ghe) => {
					total = thongtin.danhSachGhe.length * thongtin.giaVe;
					listSeats += ghe.tenGhe + ',';
					return null;
				});
				objectTemp = {
					...thongtin,
					tenHeThongRap: thongtin.danhSachGhe[0].tenHeThongRap,
					tenRap: thongtin.danhSachGhe[0].tenRap,
					listSeats: listSeats.slice(0, listSeats.length - 1),
					total,
				};
				listBooking.push(objectTemp);

				//refresh
				listSeats = '';
				total = 0;
				objectTemp = null;
				return null;
			});
		};
		handleListBooking();
	}

	const [page, setPage] = React.useState(0);
	const [rowsPerPage, setRowsPerPage] = React.useState(10);

	const handleChangePage = (event, newPage) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event) => {
		setRowsPerPage(+event.target.value);
		setPage(0);
	};
	return infoAccount ? (
		<Box style={{ width: '100%' }}>
			<Box display="flex" justifyContent="center">
				<Typography variant="h5" style={{ color: '#ffa300', marginBottom: 20 }}>
					DANH SÁCH ĐẶT VÉ
				</Typography>
			</Box>
			<TableContainer className={classes.styleTableBooking}>
				<Table stickyHeader>
					<TableHead>
						<TableRow>
							<TableCell>Tên phim</TableCell>
							<TableCell>Ngày đặt</TableCell>
							<TableCell>Giá vé</TableCell>
							<TableCell>Hệ thống rạp</TableCell>
							<TableCell>Rạp</TableCell>
							<TableCell>Số ghế</TableCell>
							<TableCell>Tổng tiền</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{listBooking.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((book, index) => (
							<TableRow key={index}>
								<TableCell>{book.tenPhim}</TableCell>
								<TableCell>{book.ngayDat}</TableCell>
								<TableCell>{book.giaVe}</TableCell>
								<TableCell>{book.tenHeThongRap}</TableCell>
								<TableCell>{book.tenRap}</TableCell>
								<TableCell>{book.listSeats}</TableCell>
								<TableCell>{book.total}</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
			<TablePagination
				rowsPerPageOptions={[10, 25, 50]}
				component="div"
				count={listBooking.length}
				rowsPerPage={rowsPerPage}
				page={page}
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
			/>
		</Box>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(Listbookedaccount));
