const styles = (theme) => {
	return {
		styleTableBooking: { maxHeight: 470 },
		[theme.breakpoints.down('sm')]: {
			styleTableBooking: { maxHeight: 470, overflowX: 'auto', width: '90vw' },
		},
		[theme.breakpoints.down('xs')]: {
			styleTableBooking: { maxHeight: 470, overflowX: 'auto', width: '85vw' },
		},
	};
};
export default styles;
