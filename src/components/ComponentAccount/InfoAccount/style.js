const styles = (theme) => {
	return {
		styleTextField: {
			width: '30vw',
		},
		styleErrorMessage: {
			color: 'red',
		},
		[theme.breakpoints.down('sm')]: {
			styleTextField: {
				width: '45vw',
			},
		},
		[theme.breakpoints.down('xs')]: {
			styleTextField: {
				width: '65vw',
			},
		},
	};
};
export default styles;
