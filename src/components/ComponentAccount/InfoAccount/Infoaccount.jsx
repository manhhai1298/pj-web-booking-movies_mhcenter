import { Box, Button, Grid, TextField, Typography, withStyles } from '@material-ui/core';
import { ErrorMessage, Form, Formik } from 'formik';
import React, { useState } from 'react';
import styles from './style';
import { swAlertService, userService } from '../../../Services/initServices';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import { createAction } from '../../../redux/actions/actionCreator';
import { SET_TOKEN_USER } from '../../../redux/actions/type';
import LoadFetch from '../../CompLoading/LoadComponent/LoadFetch';

const Infoaccount = ({ classes, infoAccount }) => {
	const [isDisableBtn, setIsDisableBtn] = useState(true);
	const dispatch = useDispatch();

	const handleChangeBtn = () => {
		if (isDisableBtn) setIsDisableBtn(false);
	};
	const handleSubmitSignUp = (value) => {
		userService
			.updateInfoAccount(value)
			.then((res) => {
				swAlertService.alertTimer('Cập nhật thành công !', 'success', 2000);
				setIsDisableBtn(true);

				const accessToken = JSON.parse(localStorage.getItem('infoUser')).accessToken;

				localStorage.setItem(
					'infoUser',
					JSON.stringify({
						accessToken: accessToken,
						taiKhoan: res.data.taiKhoan,
						hoTen: res.data.hoTen,
					})
				);
				dispatch(
					createAction(SET_TOKEN_USER, {
						accessToken: accessToken,
						taiKhoan: res.data.taiKhoan,
						hoTen: res.data.hoTen,
					})
				);
			})
			.catch((err) => {
				swAlertService.alertTimer(err, 'error', 2000);
			});
	};

	const signUpObjectValidate = yup.object().shape({
		taiKhoan: yup.string().required('* Bắt buộc nhập').min(3, '* Ít nhất 3 ký tự'),
		hoTen: yup
			.string()
			.required('* Bắt buộc nhập')
			.matches(/\b[A-Za-z]+[ ]+[A-Za-z]+\b/, ' *Nhập đầy đủ họ tên'),
		email: yup.string().required('* Bắt buộc nhập').email('* Không đúng định dạng'),
		soDt: yup
			.string()
			.required('* Bắt buộc nhập')
			.matches(/^[0-9]+$/, '* Không đúng định dạng'),
	});
	return infoAccount ? (
		<div>
			<Box display="flex" justifyContent="center">
				<Typography variant="h5" style={{ color: '#ffa300', marginBottom: 20 }}>
					THÔNG TIN TÀI KHOẢN
				</Typography>
			</Box>
			<Formik
				enableReinitialize
				initialValues={{
					taiKhoan: infoAccount.taiKhoan,
					matKhau: infoAccount.matKhau,
					hoTen: infoAccount.hoTen,
					email: infoAccount.email,
					soDt: infoAccount.soDT,
					maLoaiNguoiDung: 'KhachHang',
					maNhom: 'GP09',
				}}
				onSubmit={handleSubmitSignUp}
				validationSchema={signUpObjectValidate}
			>
				{(formikProps) => (
					<Form style={{ display: 'flex' }}>
						<Box style={{ margin: '0 auto' }}>
							<Grid container spacing={1} alignItems="center" style={{ marginBottom: 10 }}>
								<Grid item>
									<TextField
										label="Tài khoản"
										className={classes.styleTextField}
										name="taiKhoan"
										onChange={(e) => {
											formikProps.handleChange(e);
											handleChangeBtn();
										}}
										variant="outlined"
										value={formikProps.values.taiKhoan}
									/>
								</Grid>
								<Box display="block">
									<ErrorMessage
										name="taiKhoan"
										component="div"
										className={classes.styleErrorMessage}
									/>
								</Box>
							</Grid>
							<Grid container spacing={1} alignItems="center" style={{ marginBottom: 10 }}>
								<Grid item>
									<TextField
										label="Họ tên"
										className={classes.styleTextField}
										name="hoTen"
										onChange={(e) => {
											formikProps.handleChange(e);
											handleChangeBtn();
										}}
										variant="outlined"
										value={formikProps.values.hoTen}
									/>
								</Grid>
								<Box display="block">
									<ErrorMessage name="hoTen" component="div" className={classes.styleErrorMessage} />
								</Box>
							</Grid>
							<Grid container spacing={1} alignItems="center" style={{ marginBottom: 10 }}>
								<Grid item>
									<TextField
										label="Địa chỉ email"
										className={classes.styleTextField}
										name="email"
										onChange={(e) => {
											formikProps.handleChange(e);
											handleChangeBtn();
										}}
										variant="outlined"
										value={formikProps.values.email}
									/>
								</Grid>
								<Box display="block">
									<ErrorMessage name="email" component="div" className={classes.styleErrorMessage} />
								</Box>
							</Grid>
							<Grid container spacing={1} alignItems="center" style={{ marginBottom: 10 }}>
								<Grid item>
									<TextField
										label="Số điện thoại"
										className={classes.styleTextField}
										name="soDt"
										onChange={(e) => {
											formikProps.handleChange(e);
											handleChangeBtn();
										}}
										variant="outlined"
										value={formikProps.values.soDt}
									/>
								</Grid>
								<Box display="block">
									<ErrorMessage name="soDt" component="div" className={classes.styleErrorMessage} />
								</Box>
							</Grid>
							<Grid
								container
								spacing={1}
								alignItems="flex-end"
								justify="center"
								style={{ marginBottom: 10 }}
							>
								<Grid item>
									<Button
										type="submit"
										variant="contained"
										color="primary"
										style={{ margin: '15px 0 10px 0' }}
										disabled={isDisableBtn}
									>
										Cập nhật
									</Button>
								</Grid>
							</Grid>
						</Box>
					</Form>
				)}
			</Formik>
		</div>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(Infoaccount));
