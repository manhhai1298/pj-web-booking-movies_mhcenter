import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import LoadFetch from '../../CompLoading/LoadComponent/LoadFetch';
import Seat from '../Seat/Seat';

const ListSeats = () => {
	//init hook
	const listSeats = useSelector((state) => state.seat.listSeats);
	return listSeats ? (
		<Box style={{ backgroundColor: 'white', borderRadius: 7, padding: 20 }}>
			<Box
				style={{
					backgroundColor: '#fdb73b',
					borderRadius: '40px 40px 5px 5px',
					textAlign: 'center',
					marginBottom: 30,
				}}
			>
				<Typography style={{ color: 'black', fontWeight: '500' }}>Screen</Typography>
			</Box>

			{/* Seats */}
			<Box
				style={{ color: 'black', marginTop: 20 }}
				display="flex"
				justifyItems="center"
				justifyContent="center"
				flexWrap="wrap"
			>
				{listSeats.map((seat, index) => (
					<Seat key={index} seat={seat} />
				))}
			</Box>
		</Box>
	) : (
		<LoadFetch />
	);
};
export default React.memo(ListSeats);
