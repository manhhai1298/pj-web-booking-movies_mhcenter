const styles = (theme) => {
	return {
		btnBooking: {
			backgroundColor: '#fdb73b',
			borderRadius: 7,
			marginTop: 5,
			fontSize: 12,
			fontWeight: 'bold',
			width: '100%',
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			padding: '8px 0',
			'&:hover': {
				cursor: 'pointer',
				backgroundColor: '#ffa300',
			},
			'&:active': {
				cursor: 'pointer',
				backgroundColor: '#b67400',
			},
		},
	};
};
export default styles;
