import { Box, Typography, withStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { CLEAR_LIST_CHOICED, CLEAR_LIST_CHOICED_FIRST } from '../../../redux/actions/type';
import { checkoutService, swAlertService } from '../../../Services/initServices';
import { createAction } from '../../../redux/actions/actionCreator';
import styles from './style';
const ListSeatsChoiced = ({ classes }) => {
	// init hook
	const listSeatsChoiced = useSelector((state) => state.seat.listSeatsChoiced);
	const dispatch = useDispatch();
	const route = useRouteMatch();
	const history = useHistory();
	const handleListSeatsChoiced = () => {
		let listSeatChoiced = '';
		let total = 0;
		for (const seat of listSeatsChoiced) {
			listSeatChoiced += seat.tenGhe + ', ';
			total += seat.giaVe;
		}
		return { listSeatChoiced: listSeatChoiced.slice(0, listSeatChoiced.length - 2), total };
	};

	const handleBooking = () => {
		if (listSeatsChoiced.length > 0) {
			// alertChoice(title, btnText) {
			// 	return Swal.fire({
			// 		title: title,
			// 		icon: 'question',
			// 		showCancelButton: true,
			// 		confirmButtonColor: '#fdb73b',
			// 		cancelButtonColor: '#d33',
			// 		confirmButtonText: btnText,
			// 		cancelButtonText: 'Hủy',
			// 	});
			// }
			swAlertService.alertChoice('Bạn muốn đặt vé ?', 'Xác nhận', 'question').then((result) => {
				if (result.value) {
					const infoBooking = {
						maLichChieu: route.params.scheduleId,
						danhSachVe: listSeatsChoiced,
						taiKhoanNguoiDung: JSON.parse(localStorage.getItem('infoUser')).taiKhoan,
					};
					checkoutService
						.postBookingRequest(infoBooking)
						.then((res) => {
							dispatch(createAction(CLEAR_LIST_CHOICED, []));
							swAlertService
								.alertChoice('Đặt vé thành công !', 'Danh sách đặt vé', 'success')
								.then((result) => {
									if (result.value) history.push({ pathname: `/account/${2}` });
								});
						})
						.catch((err) => swAlertService.alertTimer(err, 'success', 2000));
				}
			});
		} else swAlertService.alertTimer('Bạn chưa chọn ghế !', 'warning', 2000);
	};

	useEffect(() => {
		dispatch(createAction(CLEAR_LIST_CHOICED_FIRST, []));
	}, [dispatch]);
	return (
		<Box
			style={{
				backgroundColor: 'white',
				padding: 10,
				borderRadius: 7,
				marginTop: 20,
			}}
		>
			<Box display="flex" style={{ padding: '3px 0', borderBottom: '1px dashed #ffa300' }}>
				<Box width="40%">
					<Typography variant="h6" container="h6" color="primary" style={{ color: '#ffa300' }}>
						Ghế đang đặt:
					</Typography>
				</Box>

				<Box width="60%" display="flex" justifyContent="flex-end">
					<Typography variant="h6" container="h6" style={{ marginLeft: 5 }} width="80%">
						{handleListSeatsChoiced().listSeatChoiced}
					</Typography>
				</Box>
			</Box>
			<Box display="flex" alignItems="center" style={{ padding: '3px 0' }}>
				<Box width="40%">
					<Typography variant="h6" container="h6" color="primary" style={{ color: '#ffa300' }}>
						Tổng tiền:
					</Typography>
				</Box>
				<Box width="60%" display="flex" justifyContent="flex-end">
					<Typography variant="h4" container="h4" style={{ marginLeft: 5, textDecoration: 'underline' }}>
						{handleListSeatsChoiced().total.toLocaleString() + 'đ'}
					</Typography>
				</Box>
			</Box>
			<Box display="flex" style={{ padding: '3px 0' }}>
				{/* <Box
					color="primary"
					style={{
						border: '2px solid #ffa300',
						borderRadius: 7,
						marginTop: 5,
						fontSize: 12,
						fontWeight: 'bold',
						width: '50%',
						textAlign: 'center',
					}}
				>
					<Typography style={{ fontSize: 16, color: '#ffa300' }}>Thời gian giữ ghế</Typography>
					<Typography variant="h5">04:11</Typography>
				</Box> */}
				<Box color="primary" className={classes.btnBooking} onClick={handleBooking}>
					<Typography variant="h5">Đặt vé</Typography>
				</Box>
			</Box>
		</Box>
	);
};
export default withStyles(styles)(React.memo(ListSeatsChoiced));
