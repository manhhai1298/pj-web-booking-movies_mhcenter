const style = (theme) => {
	return {
		boxSeat: {
			position: 'relative',
			'&:hover': { cursor: 'pointer' },
			'&:hover > $iconSeat': { color: '#cd850c', border: '1px solid #cd850c' },
			'&:hover > $iconSeatChoice': { color: '#26920a', border: '1px solid #26920a' },
		},
		boxSeatBooked: {
			position: 'relative',
			'&:hover > $iconSeat': { color: '#cd850c', border: '1px solid #cd850c' },
			'&:hover > $iconSeatChoice': { color: '#26920a', border: '1px solid #26920a' },
		},
		iconSeat: {
			fontSize: 35,
			margin: '6px 2px',
			color: '#ffa300',
			border: '1px solid #ffa300',
		},
		iconSeatChoice: {
			fontSize: 35,
			margin: '6px 2px',
			color: '#43b226',
			border: '1px solid #43b226',
		},
		iconSeatBooked: { fontSize: 35, margin: '6px 2px', color: '#999999', border: '1px solid #999999' },
		textSeat: {
			position: 'absolute',
			top: '50%',
			left: '50%',
			transform: 'translate(-50%, -80%)',
			fontWeight: '600',
			fontSize: 12,
			opacity: 0,
			visibility: 'hidden',
		},
		textSeatChoice: {
			position: 'absolute',
			top: '50%',
			left: '50%',
			transform: 'translate(-50%, -80%)',
			fontWeight: '600',
			fontSize: 12,
			opacity: 1,
			visibility: 'visible',
		},
	};
};
export default style;
