import { Box, Typography, withStyles } from '@material-ui/core';
import WeekendIcon from '@material-ui/icons/Weekend';
import styles from './style';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createAction } from '../../../redux/actions/actionCreator';
import { POST_SEAT_CHOICED } from '../../../redux/actions/type';

const Seat = ({ classes, seat }) => {
	// init hook
	const dispatch = useDispatch();
	const [isChoiceSeat, setIsChoiceSeat] = useState(false);

	const handleClickSeat = () => {
		dispatch(createAction(POST_SEAT_CHOICED, { maGhe: seat.maGhe, tenGhe: seat.tenGhe, giaVe: seat.giaVe }));
		setIsChoiceSeat(!isChoiceSeat);
	};
	return (
		<Box
			className={seat.daDat ? classes.boxSeatBooked : classes.boxSeat}
			onClick={seat.daDat ? null : handleClickSeat}
		>
			<WeekendIcon
				className={
					seat.daDat ? classes.iconSeatBooked : isChoiceSeat ? classes.iconSeatChoice : classes.iconSeat
				}
			/>
			<Typography
				className={seat.daDat ? classes.textSeat : isChoiceSeat ? classes.textSeatChoice : classes.textSeat}
			>
				{seat.tenGhe}
			</Typography>
		</Box>
	);
};
export default withStyles(styles)(Seat);
