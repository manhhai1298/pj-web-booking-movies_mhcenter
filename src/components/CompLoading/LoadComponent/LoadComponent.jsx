import { Box, withStyles } from '@material-ui/core';
import React from 'react';
import styles from './style';
import loadingIcon from '../../../assets/image/loadingicon/LoadingComponent.gif';
import LazyLoad from 'react-lazyload';
const LoadComponent = ({ classes }) => {
	return (
		<Box className={classes.compLoad}>
			<LazyLoad once={true}>
				<img src={loadingIcon} alt="gif-icon-loading" className={classes.gifLoad} />
			</LazyLoad>
		</Box>
	);
};
export default withStyles(styles)(React.memo(LoadComponent));
