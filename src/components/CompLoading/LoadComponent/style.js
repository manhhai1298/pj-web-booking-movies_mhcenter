const styles = (theme) => {
	return {
		compLoad: { width: '100%', margin: 0, padding: 0, top: 0, left: 0, height: '100vh', position: 'relative' },
		compLoadFetch: { width: '100%', margin: 0, padding: 0, top: 0, left: 0, height: '700px', position: 'relative' },
		gifLoad: {
			position: 'absolute',
			top: '45%',
			left: '50%',
			transform: 'translate(-50%, -50%)',
			width: '8vw',
		},
		gifLoadFetch: {
			position: 'absolute',
			top: '50%',
			left: '50%',
			transform: 'translate(-50%, -50%)',
			width: '8vw',
		},
	};
};
export default styles;
