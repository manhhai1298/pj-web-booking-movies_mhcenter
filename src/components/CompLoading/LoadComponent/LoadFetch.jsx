import { Box, withStyles } from '@material-ui/core';
import React from 'react';
import styles from './style';
import loadingIcon from '../../../assets/image/loadingicon/LoadingFetch.gif';
import LazyLoad from 'react-lazyload';
const LoadFetch = ({ classes }) => {
	return (
		<Box className={classes.compLoadFetch}>
			<LazyLoad once={true}>
				<img src={loadingIcon} alt="gif-icon-loading" className={classes.gifLoadFetch} />
			</LazyLoad>
		</Box>
	);
};
export default withStyles(styles)(React.memo(LoadFetch));
