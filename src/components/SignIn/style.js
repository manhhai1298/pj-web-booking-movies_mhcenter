const styles = (theme) => {
	return {
		compSignIn: {
			backgroundColor: '#fff',
			padding: '40px 60px 15px 60px',
			borderRadius: 15,
			textAlign: 'center',
			position: 'relative',
		},
		styleTextField: {
			width: '20vw',
		},
		btnCloseModal: {
			position: 'absolute',
			top: 0,
			right: 0,
			color: 'black',
		},
		styleErrorMessage: {
			color: 'red',
		},
		[theme.breakpoints.down('sm')]: {
			compSignIn: {
				padding: 20,
			},
			styleTextField: {
				width: '60vw',
				margin: '2px 0',
			},
			titleSignUp: {
				marginTop: 20,
				fontSize: 26,
			},
		},
		[theme.breakpoints.down('xs')]: {
			compSignIn: { padding: 10 },
			styleTextField: {
				width: '80vw',
				margin: '2px 0',
			},
			titleSignIn: {
				marginTop: 20,
				fontSize: 22,
			},
		},
	};
};
export default styles;
