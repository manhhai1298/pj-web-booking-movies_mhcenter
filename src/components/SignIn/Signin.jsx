import { Box, Button, Grid, IconButton, TextField, Typography, withStyles } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import React, { forwardRef } from 'react';
import styles from './style';
import { ErrorMessage, Form, Formik } from 'formik';
import * as yup from 'yup';
import { userService } from '../../Services/initServices';
import { swAlertService } from '../../Services/initServices';

import { useDispatch } from 'react-redux';
import { SET_TOKEN_USER } from '../../redux/actions/type';
import { createAction } from '../../redux/actions/actionCreator';

const Signin = forwardRef(({ classes, closeSignInModal, btnSignUpBonus }, ref) => {
	const signInObjectValidate = yup.object().shape({
		taiKhoan: yup.string().required('* Bắt buộc nhập'),
		matKhau: yup.string().required('* Bắt buộc nhập'),
	});
	//init Hook
	const dispatch = useDispatch();

	const handleSubmitSignIn = (value) =>
		userService
			.signIn(value)
			.then((res) => {
				localStorage.setItem(
					'infoUser',
					JSON.stringify({
						accessToken: res.data.accessToken,
						taiKhoan: res.data.taiKhoan,
						hoTen: res.data.hoTen,
					})
				);
				dispatch(
					createAction(SET_TOKEN_USER, {
						accessToken: res.data.accessToken,
						taiKhoan: res.data.taiKhoan,
						hoTen: res.data.hoTen,
					})
				);
				closeSignInModal();
				swAlertService.alertTimer('Đăng nhập thành công !', 'success', 2000);
			})
			.catch((err) => {
				swAlertService.alertTimer(err.response.data, 'error', 2000);
			});

	return (
		<div className={classes.compSignIn} ref={ref}>
			<Typography variant="h4" contained="h4" className={classes.titleSignIn}>
				Đăng nhập
			</Typography>
			<Formik
				initialValues={{ taiKhoan: '', matKhau: '' }}
				validationSchema={signInObjectValidate}
				onSubmit={handleSubmitSignIn}
			>
				{(formikProps) => (
					<Form>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item>
								<AccountCircle />
							</Grid>
							<Grid item>
								<TextField
									label="Tài khoản"
									className={classes.styleTextField}
									name="taiKhoan"
									onChange={formikProps.handleChange}
								/>
							</Grid>
							<Box display="block">
								<ErrorMessage name="taiKhoan" component="div" className={classes.styleErrorMessage} />
							</Box>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item>
								<VpnKeyIcon />
							</Grid>
							<Grid item>
								<TextField
									type="password"
									label="Mật khẩu"
									className={classes.styleTextField}
									name="matKhau"
									onChange={formikProps.handleChange}
								/>
							</Grid>
							<Box display="block">
								<ErrorMessage name="matKhau" component="div" className={classes.styleErrorMessage} />
							</Box>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end" justify="center">
							<Grid item>
								<Button
									type="submit"
									variant="contained"
									color="primary"
									style={{ margin: '15px 0 10px 0' }}
								>
									Đăng nhập
								</Button>
							</Grid>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end" justify="center">
							<Grid item>
								<Box display="flex" alignItems="center">
									<Typography>Bạn chưa có tài khoản?</Typography>
									<Button
										color="primary"
										variant="outlined"
										style={{ margin: '0 7px' }}
										onClick={btnSignUpBonus}
									>
										Đăng ký
									</Button>
								</Box>
							</Grid>
						</Grid>
					</Form>
				)}
			</Formik>

			<IconButton className={classes.btnCloseModal} onClick={closeSignInModal}>
				<HighlightOffIcon />
			</IconButton>
		</div>
	);
});
export default withStyles(styles)(React.memo(Signin));
