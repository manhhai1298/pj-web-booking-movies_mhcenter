import { Box, Container, Grid, Typography, withStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GET_LIST_THEATERGROUP, POST_THEATERGROUP_CHOOSED, POST_ID_THEATER_CHOOSE } from '../../redux/actions/type';
import ListMoviesTheater from '../ListMoviesTheater/ListMoviesTheater';
import Listtheaters from '../Listtheaters/Listtheaters';
import { createAction } from '../../redux/actions/actionCreator';
import styles from './style';
import { theaterService } from '../../Services/initServices';
import LazyLoad from 'react-lazyload';
import LoadFetch from '../CompLoading/LoadComponent/LoadFetch';
const Theatergroup = ({ classes }) => {
	//init Hook
	const dispatch = useDispatch();
	const [isSelected, setIsSelected] = useState(0);
	const idTheaterDefault = useSelector((state) => state.theater.listTheatersByIdGroup);
	//CALL API and dispatch
	useEffect(() => {
		theaterService
			.getListTheaterGroups()
			.then((res) => {
				dispatch(createAction(GET_LIST_THEATERGROUP, res.data));
			})
			.catch();
		if (idTheaterDefault.length > 0) dispatch(createAction(POST_ID_THEATER_CHOOSE, idTheaterDefault[0].maCumRap));
	}, [dispatch, idTheaterDefault]);

	// get List Theaters Group from Redux
	const listTheaterGroups = useSelector((state) => state.theater.listTheaterGroups);

	const chooseTheaterGroup = (maHeThongRap, logo, index) => {
		setIsSelected(index);
		dispatch(createAction(POST_THEATERGROUP_CHOOSED, { maHeThongRap, logo }));
	};

	return listTheaterGroups.length > 0 ? (
		<div style={{ margin: '20px 0 50px 0' }} name="ListTheaters">
			<Container maxWidth="lg">
				{/* TITLE THEATER GROUP */}
				<Typography variant="h4" contained="h4">
					CỤM RẠP
				</Typography>

				{/* LIST THEATER GROUP */}
				<Grid container>
					{listTheaterGroups.map((theaterGroup, index) => (
						<Grid
							container
							item
							lg={2}
							md={3}
							sm={4}
							xs={6}
							justify="center"
							key={theaterGroup.maHeThongRap}
						>
							<Box
								className={isSelected === index ? classes.boxTheaterSelected : classes.boxTheater}
								onClick={() =>
									isSelected === index
										? ''
										: chooseTheaterGroup(theaterGroup.maHeThongRap, theaterGroup.logo, index)
								}
							>
								<Box
									className={
										isSelected === index ? classes.boxImgTheaterSelected : classes.boxImgTheater
									}
								>
									<LazyLoad once={true}>
										<img
											src={
												theaterGroup.logo.indexOf('https') === -1
													? theaterGroup.logo.replace('http', 'https')
													: null
											}
											alt={theaterGroup.biDanh}
											className={classes.imgTheater}
										/>
									</LazyLoad>

									<Typography variant="h6" contained="h6" className={classes.titleTheater}>
										{theaterGroup.tenHeThongRap}
									</Typography>
								</Box>
							</Box>
						</Grid>
					))}
				</Grid>
				<Grid container>
					<Grid container item md={6} justify="center">
						<Listtheaters />
					</Grid>
					<Grid container item md={6} justify="center">
						<ListMoviesTheater />
					</Grid>
				</Grid>
			</Container>
		</div>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(Theatergroup));
