const styles = (theme) => {
	return {
		boxTheater: {
			textAlign: 'center',
			margin: '15px 0',
			opacity: 0.45,
			transition: 'all 0.4s',
			'&:hover': { opacity: 1, transition: 'all 0.4s', cursor: 'pointer' },
		},
		boxTheaterSelected: {
			textAlign: 'center',
			margin: '15px 0',
			opacity: 1,
		},
		boxImgTheater: {
			position: 'relative',
			textAlign: 'center',
			padding: 14,
			width: 100,
			height: 100,
			backgroundColor: theme.palette.primary.main,
			borderRadius: '50%',
			'&:hover': {
				'& $imgTheater': {
					animation: '$customizeBounce 1.5s ease-out both',
					animationIterationCount: 'infinite',
				},
			},
		},
		boxImgTheaterSelected: {
			position: 'relative',
			textAlign: 'center',
			padding: 14,
			width: 100,
			height: 100,
			backgroundColor: theme.palette.primary.main,
			borderRadius: '50%',
		},
		'@keyframes customizeBounce': {
			'0% ': { transform: 'scale(1,1)    translateY(0)' },
			'10%': { transform: 'scale(1.1,.9) translateY(0)' },
			'30%': { transform: 'scale(.9,1) translateY(-10px)' },
			'50% ': { transform: 'scale(1,1)    translateY(0)' },
			'100%': { transform: 'scale(1,1)    translateY(0)' },
		},
		imgTheater: {
			position: 'absolute',
			// center
			margin: 'auto',
			left: 0,
			right: 0,
			top: 0,
			bottom: 0,
			// center
			width: 100,
			height: 100,
			borderRadius: '50%',
		},
		titleTheater: {
			position: 'absolute',
			// center
			margin: '0 auto',
			left: 0,
			right: 0,
			// center
			bottom: -7,
			backgroundColor: theme.palette.primary.main,
			borderRadius: 20,
			fontSize: 14,
			width: 130,
		},
	};
};
export default styles;
