import React, { useEffect } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import carousel1 from '../../assets/image/carousel/carousel1.jpg';
import carousel2 from '../../assets/image/carousel/carousel2.jpg';
import carousel3 from '../../assets/image/carousel/carousel3.jpg';
import carousel4 from '../../assets/image/carousel/carousel4.jpg';
import carousel5 from '../../assets/image/carousel/carousel5.jpg';
import { withStyles } from '@material-ui/core';
import { WOW } from 'wowjs';
import 'animate.css/animate.css';
import style from './style';
import Swal from 'sweetalert2';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded';
import ArrowForwardIosRoundedIcon from '@material-ui/icons/ArrowForwardIosRounded';
import CarouselItem from './CarouselItem';

const listMovieCarousel = [
	{
		movieId: '3634',
		movieName: 'Dự Án Siêu Năng Lực',
		movieType: 'Hành động - Phiêu lưu - Viễn tưởng',
		movieDesc:
			'Dự Án Siêu Năng Lực kể về một cựu quân nhân, một thiếu niên và một cảnh sát va chạm ở New Orleans khi họ săn lùng nguồn gốc đằng sau một viên thuốc mới nguy hiểm cấp cho người dùng một siêu năng lực tạm thời.',
		movieImage: carousel1,
		movieTrailer: 'https://www.youtube.com/embed/xw1vQgVaYNQ',
	},
	{
		movieId: '3351',
		movieName: 'King Kong',
		movieType: 'Khoa học - Viễn tưởng - Hành động',
		movieDesc:
			'Kong - Đảo Đầu lâu là một phim quái vật viễn tưởng của Mỹ ra mắt năm 2017 do Jordan Vogt-Roberts đạo diễn, với phần kịch bản được thực hiện bởi Dan Gilroy, Max Borenstein và Derek Connolly từ cốt truyện của John Gatins và Gilroy.',
		movieImage: carousel2,
		movieTrailer: 'https://www.youtube.com/embed/EuzY9pGnAlQ',
	},
	{
		movieId: '3575',
		movieName: 'Tiếng Gọi Nơi Hoang Dã',
		movieType: 'Phiêu lưu - Tâm lý - Tình cảm',
		movieDesc:
			'Bộ phim dựa trên tác phẩm văn học nổi tiếng cùng tên của Jack London kể về một con chó tên Buck đã được thuần hóa, cưng chiều. Nhưng một loạt các sự kiện xảy ra khi Buck bị bắt khỏi trang trại để trở thành chó kéo xe ở khu vực Alaska lạnh giá, trong giai đoạn mọi người đổ xô đi tìm vàng thế kỷ 19, thiên nhiên nguyên thủy đã đánh thức bản năng của Buck.',
		movieImage: carousel3,
		movieTrailer: 'https://www.youtube.com/embed/5P8R2zAhEwg',
	},
	{
		movieId: '3054',
		movieName: 'GRAY HOUND',
		movieType: 'Hành động - Chiến tranh',
		movieDesc:
			'Phim lấy cảm hứng từ sự kiện lịch sử có thật - trận chiến Đại Tây Dương. Với sự góp mặt của Tom Hanks vào vai một thuyền trưởng lần đầu tiên dẫn một đoàn tàu Đồng Minh chở hàng ngàn binh lính và tiếp tế băng qua vùng biển nguy hiểm ở Trung Đại Tây Dương đến tiền tuyến của thế chiến II. Nhưng trước khi đến được đó, những kẻ thù dần xuất hiện.',
		movieImage: carousel4,
		movieTrailer: 'https://www.youtube.com/embed/llr6SQpBzWs',
	},
	{
		movieId: '1532',
		movieName: 'Avengers: Infinity War',
		movieType: 'Khoa học - Viễn tưởng - Hành động',
		movieDesc:
			'Biệt đội siêu anh hùng Avengers và những đồng minh sẽ phải sẵn sàng hi sinh tính mạng để chống lại siêu ác nhân hùng mạnh Thanos trước khi hắn phá huỷ mọi thứ và đặt dấu chấm hết cho vũ trụ.',
		movieImage: carousel5,
		movieTrailer: 'https://www.youtube.com/embed/DKqu9qc-5f4',
	},
];

const Carousel = ({ classes }) => {
	//Init Hook

	//Customize ArrowButton Slick
	const SampleNextArrow = (props) => {
		return <ArrowForwardIosRoundedIcon className={classes.nextBtnSlick} onClick={props.onClick} />;
	};

	const SamplePrevArrow = (props) => {
		return <ArrowBackIosRoundedIcon className={classes.prevBtnSlick} onClick={props.onClick} />;
	};
	const initialWowCarousel = () => {
		const wowCarousel = new WOW({
			boxClass: 'wowCarousel',
			live: false,
			// mobile: true,
		});
		wowCarousel.init();
	};

	const settings1 = {
		autoplay: true,
		autoplaySpeed: 5000,
		dots: false,
		speed: 1300,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: <SampleNextArrow />,
		prevArrow: <SamplePrevArrow />,
		beforeChange: initialWowCarousel,
	};

	const handleWidthTrailer = () => {
		const widthScreen = window.innerWidth;
		if (widthScreen <= 600) return `width="280" height="220"`;
		else if (widthScreen > 600 && widthScreen <= 960) return `width="540" height="380"`;
		else if (widthScreen > 960 && widthScreen <= 1280) return `width="864" height="480"`;
		else if (widthScreen > 1280 && widthScreen <= 1920) return `width="1024" height="576"`;
		else if (widthScreen > 1920) return `width="1366" height="768"`;
		return `width="854" height="480"`;
	};

	//View Trailer Movie
	const handleViewTrailer = (url) => {
		Swal.fire({
			showCancelButton: true,
			cancelButtonText: 'Đóng',
			cancelButtonColor: '#d33',
			showConfirmButton: false,
			customClass: classes.styleAlertTrailer,
			html: `<iframe ${handleWidthTrailer()} src="${url}?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`,
		});
	};
	useEffect(() => {
		initialWowCarousel();
	}, []);
	return (
		<div>
			<Slider {...settings1}>
				{listMovieCarousel.map((movie, index) => (
					<CarouselItem
						key={index}
						carouselItem={movie}
						handleViewTrailer={() => handleViewTrailer(movie.movieTrailer)}
					/>
				))}
			</Slider>
		</div>
	);
};
export default withStyles(style)(React.memo(Carousel));
