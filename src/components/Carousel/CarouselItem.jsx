import { Box, Button, Typography, withStyles } from '@material-ui/core';
import React from 'react';
import styles from './style';
import PlayCircleOutlineRoundedIcon from '@material-ui/icons/PlayCircleOutlineRounded';
import MovieCreationRoundedIcon from '@material-ui/icons/MovieCreationRounded';
import { useHistory } from 'react-router-dom';
import LazyLoad from 'react-lazyload';

const CarouselItem = ({ classes, carouselItem, handleViewTrailer }) => {
	const history = useHistory();

	const handleBooking = () => {
		//Get Token user signin from redux
		history.push({ pathname: `/detail/${carouselItem.movieId}` });
	};
	return (
		<Box className={classes.styleCarouselItem} index={2}>
			<LazyLoad once={true}>
				<img src={carouselItem.movieImage} alt="phim-chieu-rap-2" className={classes.styleImage} />
			</LazyLoad>
			{/* color cover image carousel  */}
			<Box className={classes.boxColorImageCarousel}></Box>
			{/* Content Carousel */}
			<Box className={classes.boxContentCarousel}>
				<Typography
					variant="h3"
					component="h3"
					className={`wowCarousel fadeInRight ${classes.styleTitleCarousel}`}
					data-wow-delay="0.6s"
				>
					{carouselItem.movieName}
				</Typography>
				<Typography
					variant="h6"
					component="h6"
					color="primary"
					className={`wowCarousel fadeInRight ${classes.styleTopicCarousel}`}
					data-wow-delay="1s"
				>
					{carouselItem.movieType}
				</Typography>
				<Typography className={`wowCarousel fadeInRight ${classes.styleContentCarousel}`} data-wow-delay="1.5s">
					{carouselItem.movieDesc}
				</Typography>

				<Button
					key={carouselItem.movieId}
					variant="contained"
					color="primary"
					classes={{ root: classes.styleBtnTrailer }}
					style={{
						margin: 10,
						backgroundColor: 'black',
						color: '#fdb73b',
						border: '2px solid #fdb73b',
					}}
					className="wowCarousel fadeInLeft"
					data-wow-delay="2s"
					onClick={handleBooking}
				>
					<MovieCreationRoundedIcon style={{ marginRight: 3 }} />
					Booking
				</Button>
				<Button
					variant="contained"
					color="primary"
					classes={{ root: classes.styleBtnTrailer }}
					className="wowCarousel fadeInRight"
					data-wow-delay="2s"
					onClick={handleViewTrailer}
				>
					<PlayCircleOutlineRoundedIcon style={{ marginRight: 3 }} />
					Trailer
				</Button>
			</Box>
		</Box>
	);
};
export default withStyles(styles)(React.memo(CarouselItem));
