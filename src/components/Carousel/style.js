const style = (theme) => {
	return {
		styleCarouselItem: {
			position: 'relative',
		},
		styleImage: {
			position: 'relative',
			width: '100%',
			height: 620,
		},
		boxColorImageCarousel: {
			position: 'absolute',
			left: 0,
			top: 0,
			width: '100%',
			height: '100%',
			background: 'linear-gradient(50deg,#000,rgba(0,0,0,.8) 40%,transparent)',
		},

		// Content Carousel
		boxContentCarousel: {
			position: 'absolute',
			top: '50%',
			left: 110,
			transform: 'translate(0,-50%)',
			color: '#fff',
			width: '29vw',
		},
		styleContentCarousel: {
			margin: '25px 0 15px 0',
		},
		//Button trailer
		styleBtnTrailer: {
			color: 'black',
			borderRadius: 20,
			width: 120,
		},

		styleAlertTrailer: {
			padding: theme.spacing(1),
			width: 'auto',
			height: 'auto',
			'& .swal2-content': {
				padding: 0,
			},
		},

		//btn Slick
		prevBtnSlick: {
			position: 'absolute',
			padding: 3,
			zIndex: 12,
			left: 20,
			top: '48%',
			backgroundColor: theme.palette.primary.main,
			fontSize: 14,
			borderRadius: '50%',
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
				cursor: 'pointer',
			},
		},
		nextBtnSlick: {
			position: 'absolute',
			padding: 3,
			right: 20,
			top: '48%',
			backgroundColor: theme.palette.primary.main,
			fontSize: 14,
			borderRadius: '50%',
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
				cursor: 'pointer',
			},
		},
		[theme.breakpoints.down('md')]: {
			styleImage: {
				height: 500,
			},
			// Content Carousel
			boxContentCarousel: {
				width: '31vw',
			},
		},
		[theme.breakpoints.down('sm')]: {
			styleImage: {
				height: 400,
			},
			// Content Carousel
			boxContentCarousel: {
				left: 70,
				width: '45vw',
			},
			styleTitleCarousel: {
				fontSize: 22,
			},
			styleTopicCarousel: {
				fontSize: 15,
			},
			styleContentCarousel: {
				fontSize: 13,
			},
		},
		[theme.breakpoints.down('xs')]: {
			styleImage: {
				height: 280,
			},
			// Content Carousel
			boxContentCarousel: {
				left: 50,
				width: '70vw',
			},
			//Button trailer
			styleBtnTrailer: {
				width: 85,
				fontSize: '11px',
			},
			boxColorImageCarousel: {
				position: 'absolute',
				left: 0,
				top: 0,
				width: '100%',
				height: '100%',
				background: 'linear-gradient(50deg,#000,rgba(0,0,0,.7) 60%,transparent)',
			},
			styleTitleCarousel: {
				fontSize: 18,
			},
			styleTopicCarousel: {
				fontSize: 13,
			},
			styleContentCarousel: {
				margin: '10px 0 5px 0',
				fontSize: 11,
			},
		},
	};
};
export default style;
