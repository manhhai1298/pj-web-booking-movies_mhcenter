import {
	Box,
	Button,
	Grid,
	GridList,
	GridListTile,
	Modal,
	Table,
	TableBody,
	TableCell,
	TableRow,
	Typography,
	withStyles,
} from '@material-ui/core';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import LoadFetch from '../CompLoading/LoadComponent/LoadFetch';
import styles from './style';
import Signin from '../SignIn/Signin';
import Signup from '../SignUp/Signup';
const ScheduleDetailMovie = ({ classes, infoShowMovie }) => {
	const [isOpenSignIn, setIsOpenSignIn] = useState(false);
	const [isOpenSignUp, setIsOpenSignUp] = useState(false);
	const [listTime, setListTime] = useState({ listTime: [], theaterName: '', theaterLogo: '' });
	const [listTheater, setListTheater] = useState({ listTheater: [], theaterName: '', theaterLogo: '', date: '' });
	const [theaterChooseId, setTheaterChosseId] = useState('');
	const test = useRef(false);
	const route = useRouteMatch();
	const userSignIn = useSelector((state) => state.user.rdInfoUser);
	const history = useHistory();

	const handleChoiceSchedule = (scheduleId) => {
		if (userSignIn.accessToken) history.push({ pathname: `/checkout/${scheduleId}` });
		else setIsOpenSignIn(true);
	};

	const handleClickDate = useCallback(
		(dateInput) => {
			const dateTemp = dateInput.substring(3, 6) + '-' + dateInput.substring(0, 2);
			const listTheaterTemp = [];
			if (infoShowMovie.heThongRapChieu !== undefined) {
				infoShowMovie.heThongRapChieu
					.find((htRap) => htRap.maHeThongRap === theaterChooseId)
					.cumRapChieu.map((cumRap) =>
						cumRap.lichChieuPhim.map((lichChieu) => {
							const date =
								lichChieu.ngayChieuGioChieu.substring(8, 10) +
								'-' +
								lichChieu.ngayChieuGioChieu.substring(5, 7);
							if (date === dateInput && listTheaterTemp.indexOf(cumRap) === -1)
								listTheaterTemp.push(cumRap);
							return null;
						})
					);
			}

			setListTheater({
				listTheater: listTheaterTemp,
				theaterName: listTime.theaterName,
				theaterLogo: listTime.theaterLogo,
				date: dateTemp,
			});
		},
		[theaterChooseId, listTheater]
	);

	const handleClickDate2 = useCallback(
		(dateInput, theaterName, theaterLogo, theaterId) => {
			const dateTemp = dateInput.substring(3, 6) + '-' + dateInput.substring(0, 2);
			const listTheaterTemp = [];
			if (infoShowMovie.heThongRapChieu !== undefined) {
				infoShowMovie.heThongRapChieu
					.find((htRap) => htRap.maHeThongRap === theaterId)
					.cumRapChieu.map((cumRap) =>
						cumRap.lichChieuPhim.map((lichChieu) => {
							const date =
								lichChieu.ngayChieuGioChieu.substring(8, 10) +
								'-' +
								lichChieu.ngayChieuGioChieu.substring(5, 7);
							if (date === dateInput && listTheaterTemp.indexOf(cumRap) === -1)
								listTheaterTemp.push(cumRap);

							return null;
						})
					);
			}
			setListTheater({
				listTheater: listTheaterTemp,
				theaterName: theaterName,
				theaterLogo: theaterLogo,
				date: dateTemp,
			});
		},
		[theaterChooseId]
	);
	const handleClickTheater = (theaterId, theaterName, theaterLogo) => {
		test.current = true;
		const listTimeTemp = [];
		setTheaterChosseId(theaterId);
		infoShowMovie.heThongRapChieu
			.find((htRap) => htRap.maHeThongRap === theaterId)
			.cumRapChieu.map((cumRap) =>
				cumRap.lichChieuPhim.map((lichChieu) => {
					const date =
						lichChieu.ngayChieuGioChieu.substring(8, 10) +
						'-' +
						lichChieu.ngayChieuGioChieu.substring(5, 7);
					if (listTimeTemp.indexOf(date) === -1) listTimeTemp.push(date);
					return null;
				})
			);
		setListTime({ listTime: listTimeTemp, theaterName, theaterLogo });
		handleClickDate2(listTimeTemp[0], theaterName, theaterLogo, theaterId);
	};

	const handleClickTheater2 = useCallback(
		(theaterId, theaterName, theaterLogo, listInput) => {
			const listTimeTemp = [];
			setTheaterChosseId(theaterId);
			listInput.heThongRapChieu[0].cumRapChieu.map((cumRap) =>
				cumRap.lichChieuPhim.map((lichChieu) => {
					const date =
						lichChieu.ngayChieuGioChieu.substring(8, 10) +
						'-' +
						lichChieu.ngayChieuGioChieu.substring(5, 7);
					if (listTimeTemp.indexOf(date) === -1) listTimeTemp.push(date);
					return null;
				})
			);

			setListTime({ listTime: listTimeTemp, theaterName, theaterLogo });
			handleClickDate2(listTimeTemp[0], theaterName, theaterLogo, theaterId);
		},
		[handleClickDate2]
	);

	useEffect(() => {
		if (!test.current) {
			if (infoShowMovie.heThongRapChieu[0] !== undefined) {
				handleClickTheater2(
					infoShowMovie.heThongRapChieu[0].maHeThongRap,
					infoShowMovie.heThongRapChieu[0].tenHeThongRap,
					infoShowMovie.heThongRapChieu[0].logo,
					infoShowMovie
				);
			}
		}
	}, [route, handleClickTheater2]);

	return infoShowMovie ? (
		<div>
			<Box style={{ textAlign: 'center' }} className="scheduleMovie">
				<Typography variant="h4" container="h4" className={classes.textDetailItem} color="primary">
					Lịch chiếu
				</Typography>
			</Box>
			<Box style={{ backgroundColor: '#fff', padding: '10px 0', borderRadius: 7 }}>
				<Grid container>
					{infoShowMovie.heThongRapChieu[0] !== undefined ? (
						<>
							<Grid container item sm={4} justify="center">
								<Typography variant="h5" className={classes.textChoiceTheater}>
									Chọn rạp
								</Typography>
								<Box className={classes.tableTheater}>
									<Table>
										<TableBody>
											{infoShowMovie.heThongRapChieu.map((htRap) => (
												<TableRow
													key={htRap.maHeThongRap}
													onClick={() =>
														handleClickTheater(
															htRap.maHeThongRap,
															htRap.tenHeThongRap,
															htRap.logo
														)
													}
													className={
														theaterChooseId === htRap.maHeThongRap
															? classes.rowTheaterSelected
															: classes.rowTheater
													}
												>
													<TableCell style={{ display: 'flex' }} align="center">
														<img
															src={
																htRap.logo.indexOf('https') === -1
																	? htRap.logo.replace('http', 'https')
																	: null
															}
															alt="theater-img"
															style={{
																width: '60px',
																height: '60px',
																marginRight: 10,
															}}
														/>
														<Box display="flex" alignItems="center">
															<Typography variant="h6" contained="h6" color="primary">
																{htRap.tenHeThongRap}
															</Typography>
														</Box>
													</TableCell>
												</TableRow>
											))}
										</TableBody>
									</Table>
								</Box>
							</Grid>
							<Box className={classes.boxTitleChoiceSchedule}>
								<Typography variant="h5" className={classes.textChoiceTheater}>
									Chọn ngày giờ chiếu
								</Typography>
							</Box>
							<Grid container item sm={8}>
								<Box style={{ color: 'black', width: '100%' }}>
									<Box mr={2} className={classes.boxDateScheduleMobile}>
										<GridList
											cellHeight="auto"
											style={{ flexWrap: 'nowrap', padding: '15px 0' }}
											cols={3}
											component="div"
										>
											{listTime.listTime.map((date, index) => (
												<GridListTile
													key={index}
													rows={5}
													component="div"
													className={classes.styleCellDate}
												>
													<Box>
														<Typography
															variant="h6"
															className={
																listTheater.date ===
																date.substring(3, 6) + '-' + date.substring(0, 2)
																	? classes.itemDateMovieSeleted
																	: classes.itemDateMovie
															}
															onClick={
																listTheater.date ===
																date.substring(3, 6) + '-' + date.substring(0, 2)
																	? null
																	: () => {
																			handleClickDate(date);
																	  }
															}
														>
															{date}
														</Typography>
													</Box>
												</GridListTile>
											))}
										</GridList>
									</Box>
									<Box mr={2} className={classes.boxDateScheduleTablet}>
										<GridList
											cellHeight="auto"
											style={{ flexWrap: 'nowrap', padding: '15px 0' }}
											cols={4}
											component="div"
										>
											{listTime.listTime.map((date, index) => (
												<GridListTile
													key={index}
													rows={5}
													component="div"
													className={classes.styleCellDate}
												>
													<Box>
														<Typography
															variant="h6"
															className={
																listTheater.date ===
																date.substring(3, 6) + '-' + date.substring(0, 2)
																	? classes.itemDateMovieSeleted
																	: classes.itemDateMovie
															}
															onClick={
																listTheater.date ===
																date.substring(3, 6) + '-' + date.substring(0, 2)
																	? null
																	: () => {
																			handleClickDate(date);
																	  }
															}
														>
															{date}
														</Typography>
													</Box>
												</GridListTile>
											))}
										</GridList>
									</Box>
									<Box mr={2} className={classes.boxDateSchedule}>
										<GridList
											cellHeight="auto"
											style={{ flexWrap: 'nowrap', padding: '15px 0' }}
											cols={6}
											component="div"
										>
											{listTime.listTime.map((date, index) => (
												<GridListTile
													key={index}
													rows={5}
													component="div"
													className={classes.styleCellDate}
												>
													<Box>
														<Typography
															variant="h6"
															className={
																listTheater.date ===
																date.substring(3, 6) + '-' + date.substring(0, 2)
																	? classes.itemDateMovieSeleted
																	: classes.itemDateMovie
															}
															onClick={
																listTheater.date ===
																date.substring(3, 6) + '-' + date.substring(0, 2)
																	? null
																	: () => {
																			handleClickDate(date);
																	  }
															}
														>
															{date}
														</Typography>
													</Box>
												</GridListTile>
											))}
										</GridList>
									</Box>
									<Box className={classes.tableTheater}>
										<Table>
											<TableBody>
												{listTheater.listTheater.map((theater, index) => (
													<TableRow key={index}>
														<TableCell>
															<Box
																style={{
																	padding: '10px 0',
																}}
															>
																<Box
																	style={{
																		display: 'flex',
																	}}
																>
																	<Box>
																		<img
																			src={
																				listTheater.theaterLogo.indexOf(
																					'https'
																				) === -1
																					? listTheater.theaterLogo.replace(
																							'http',
																							'https'
																					  )
																					: null
																			}
																			alt="theater-img"
																			style={{
																				width: '60px',
																				height: '60px',
																				marginRight: 10,
																			}}
																		/>

																		<Box
																			color="primary"
																			style={{
																				border: '2px solid #fdb73b',
																				borderRadius: 4,
																				fontSize: 12,
																				margin: '5px 0 10px 0',
																				fontWeight: 'bold',
																				padding: '0 0 0 5px',
																			}}
																		>
																			<Typography
																				variant="h6"
																				style={{ fontSize: 12 }}
																			>
																				2D Digital
																			</Typography>
																		</Box>
																	</Box>
																	<Box>
																		<Typography
																			variant="h5"
																			contained="h4"
																			color="primary"
																		>
																			{listTheater.theaterName}
																		</Typography>
																		<Typography>{theater.tenCumRap}</Typography>
																	</Box>
																</Box>
																<Box>
																	<Typography variant="h6" container="h6">
																		Giờ chiếu:
																	</Typography>
																	{theater.lichChieuPhim.map((lichChieu, index) =>
																		lichChieu.ngayChieuGioChieu.substring(5, 10) ===
																		listTheater.date ? (
																			<Button
																				key={index}
																				variant="outlined"
																				style={{
																					margin: 5,
																					backgroundColor: '#fdb73b',
																					width: 99,
																				}}
																				onClick={() =>
																					handleChoiceSchedule(
																						lichChieu.maLichChieu
																					)
																				}
																			>
																				{lichChieu.ngayChieuGioChieu.substring(
																					11,
																					16
																				)}
																			</Button>
																		) : (
																			''
																		)
																	)}
																</Box>
															</Box>
														</TableCell>
													</TableRow>
												))}
											</TableBody>
										</Table>
									</Box>
								</Box>
							</Grid>

							<Modal
								open={isOpenSignIn}
								onClose={() => setIsOpenSignIn(false)}
								aria-labelledby="simple-modal-title"
								aria-describedby="simple-modal-description"
								style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
								disableAutoFocus={true}
							>
								<Signin
									closeSignInModal={() => setIsOpenSignIn(false)}
									btnSignUpBonus={() => {
										setIsOpenSignIn(false);
										setIsOpenSignUp(true);
									}}
								/>
							</Modal>
							<Modal
								open={isOpenSignUp}
								onClose={() => setIsOpenSignUp(false)}
								aria-labelledby="simple-modal-title"
								aria-describedby="simple-modal-description"
								style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
								disableAutoFocus={true}
							>
								<Signup closeSignUpModal={() => setIsOpenSignUp(false)} />
							</Modal>
						</>
					) : (
						<Box style={{ height: '500px', color: 'black' }}>
							<Typography variant="h5" style={{ marginLeft: 10 }}>
								Chưa có lịch chiếu
							</Typography>
						</Box>
					)}
				</Grid>
			</Box>
		</div>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(ScheduleDetailMovie));
