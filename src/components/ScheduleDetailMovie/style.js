const styles = (theme) => {
	return {
		rowTheater: {
			opacity: 0.45,
			transition: 'all 0.4s',
			'&:hover': {
				cursor: 'pointer',
				opacity: 1,
				transition: 'all 0.4s',
			},
		},
		rowTheaterSelected: {
			opacity: 1,
			transition: 'all 0.4s',
		},

		itemDateMovie: {
			transition: 'all 0.4s',
			'&:hover': {
				cursor: 'pointer',
				transition: 'all 0.4s',
				fontSize: 25,
				color: '#ffa300',
			},
		},
		itemDateMovieSeleted: { fontSize: 25, color: '#ffa300', transition: 'all 0.4s' },

		styleCellDate: {
			'& .MuiGridListTile-tile': {
				display: 'flex',
				alignItems: 'center',
			},
		},
		tableTheater: { height: 500, overflow: 'auto', width: '100%' },
		textChoiceTheater: {
			color: 'black',
			display: 'none',
		},
		boxTitleChoiceSchedule: {
			textAlign: 'center',
		},
		boxDateSchedule: {},
		boxDateScheduleTablet: {},
		boxDateScheduleMobile: {},
		[theme.breakpoints.down('xl')]: {
			boxDateSchedule: { display: 'block' },
			boxDateScheduleMobile: { display: 'none' },
			boxDateScheduleTablet: {
				display: 'none',
			},
		},
		[theme.breakpoints.down('lg')]: {
			boxDateSchedule: { display: 'block' },
			boxDateScheduleMobile: { display: 'none' },
			boxDateScheduleTablet: {
				display: 'none',
			},
		},
		[theme.breakpoints.down('md')]: {
			boxDateSchedule: { display: 'block' },
			boxDateScheduleMobile: { display: 'none' },
			boxDateScheduleTablet: {
				display: 'none',
			},
		},
		[theme.breakpoints.down('sm')]: {
			boxDateSchedule: { display: 'none' },
			boxDateScheduleMobile: { display: 'none' },
			boxDateScheduleTablet: {
				display: 'block',
			},
		},
		[theme.breakpoints.down('xs')]: {
			boxDateSchedule: { display: 'none' },
			boxDateScheduleTablet: { display: 'none' },
			boxDateScheduleMobile: {
				display: 'block',
				marginLeft: 30,
			},
			tableTheater: { height: 'auto' },
			textChoiceTheater: {
				margin: '0 10px',
				color: 'black',
				display: 'block',
			},
			boxTitleChoiceSchedule: {
				width: '100%',
			},
		},
	};
};
export default styles;
