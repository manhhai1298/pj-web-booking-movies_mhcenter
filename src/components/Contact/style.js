import BackgroundMovie from '../../assets/image/background/bgContact.jpg';
const styles = (theme) => {
	return {
		colorCoverContact: {},
		boxCompContact: {
			height: 550,
			zIndex: 1,
			position: 'relative',
			backgroundImage: `url(${BackgroundMovie})`,
			backgroundSize: '100% 100%',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundAttachment: 'fixed',
			backgroundColor: 'black',
			'& $colorCoverContact': {
				position: 'absolute',
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				zIndex: -2,
				backgroundColor: 'rgb(0,0,0,0.85)',
			},
		},

		styleTextField: {
			height: 40,
			width: '19vw',
			backgroundColor: '#fff',
			borderRadius: '20px 0 0 20px',
			padding: theme.spacing(0, 2),
			marginLeft: theme.spacing(3),
		},
		btnEmail: {
			backgroundColor: theme.palette.primary.main,
			height: 40,
			borderRadius: '0 20px 20px 0',
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
			},
		},

		styleSliderSetting: {
			paddingTop: 13,
			paddingLeft: 11,
			paddingRight: 11,
			paddingBottom: 9,
		},

		boxSlider: {
			width: 240,
			position: 'relative',
			margin: '0 15px 0 30px',
		},

		imgSlider: {
			width: '100%',
			height: 430,
		},

		styleImgLogoOS: {
			width: '30%',
		},

		descDownloadMobileApp: {
			color: '#fff',
			marginBottom: 10,
		},
		// Responsive
		[theme.breakpoints.down('md')]: {
			boxInputEmail: { marginRight: 50 },
			boxSlider: {
				width: 220,
			},

			imgSlider: {
				height: 390,
			},
			styleSliderSetting: {
				paddingTop: 11,
				paddingBottom: 8,
			},
			styleImgLogoOS: {
				width: '45%',
			},
			styleTextField: {
				width: '22vw',
			},
		},
		[theme.breakpoints.down('sm')]: {
			boxInputEmail: { marginRight: 0 },
			gridInputEmail: {
				textAlign: 'center',
			},
			gridMobileApp: { textAlign: 'center' },
			boxSlider: {
				width: 170,
			},

			imgSlider: {
				height: 310,
			},
			styleSliderSetting: {
				paddingTop: 8,
				paddingBottom: 5,
			},
			styleImgLogoOS: {
				width: '20%',
			},
			styleTextField: {
				width: '32vw',
			},
		},

		[theme.breakpoints.down('xs')]: {
			boxCompContact: {
				backgroundSize: '100% 35%',
				backgroundRepeat: 'repeat',
			},
			boxInputEmail: { marginRight: 0 },
			gridInputEmail: {
				textAlign: 'center',
			},
			gridMobileApp: { textAlign: 'center' },
			boxSlider: {
				width: 120,
				margin: '0px 10px 0px 0px',
			},

			imgSlider: {
				height: 210,
			},
			styleSliderSetting: {
				paddingTop: 5,
				paddingRight: 7,
				paddingLeft: 7,
				paddingBottom: 1,
			},
			styleImgLogoOS: {
				width: '40%',
			},
			styleTextField: {
				height: 30,
				width: '45vw',
			},
			btnEmail: {
				backgroundColor: theme.palette.primary.main,
				height: 30,
				paddingRight: 7,
				borderRadius: '0 20px 20px 0',
				'&:hover': {
					backgroundColor: theme.palette.primary.dark,
				},
			},
			btnAccept: {
				display: 'none',
			},
			descDownloadMobileApp: {
				display: 'none',
			},
			containerPadding: {
				padding: '0 10px',
			},
			titleContact: {
				fontSize: 16,
			},
		},
	};
};
export default styles;
