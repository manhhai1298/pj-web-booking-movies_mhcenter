import { Box, Button, Container, Grid, TextField, Typography, withStyles } from '@material-ui/core';
import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import React, { useEffect } from 'react';
import styles from './style';
import iphoneXScreen from '../../assets/image/devices/iphonexScreen.png';
import ios from '../../assets/image/devices/appstore.png';
import android from '../../assets/image/devices/playstore.png';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import slide2 from '../../assets/image/devices/slider/slide2.jpg';
import slide3 from '../../assets/image/devices/slider/slide3.jpg';
import slide4 from '../../assets/image/devices/slider/slide4.jpg';
import slide5 from '../../assets/image/devices/slider/slide5.jpg';
import slide6 from '../../assets/image/devices/slider/slide6.jpg';
import slide7 from '../../assets/image/devices/slider/slide7.jpg';
import slide8 from '../../assets/image/devices/slider/slide8.jpg';
import slide9 from '../../assets/image/devices/slider/slide9.jpg';
import slide10 from '../../assets/image/devices/slider/slide10.jpg';
import slide11 from '../../assets/image/devices/slider/slide11.jpg';
import slide12 from '../../assets/image/devices/slider/slide12.jpg';
import slide13 from '../../assets/image/devices/slider/slide13.jpg';
import slide14 from '../../assets/image/devices/slider/slide14.jpg';
import slide15 from '../../assets/image/devices/slider/slide15.jpg';
import { swAlertService } from '../../Services/initServices';
import { Link } from 'react-router-dom';
import { WOW } from 'wowjs';
import { Form, Formik } from 'formik';
import LazyLoad from 'react-lazyload';

const Contact = ({ classes }) => {
	const settings = {
		autoplay: true,
		autoplaySpeed: 3000,
		speed: 700,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
	};

	const handleSignUpEmail = (value) => {
		if (!value.email) swAlertService.alertTimer('Không được để trống email !', 'warning', 2000);
		else {
			const regexEmail = /\S+@\S+\.\S+/;
			if (regexEmail.test(value.email)) {
				swAlertService.alertTimer('Đăng ký nhận tin thành công !', 'success', 2000);
			} else swAlertService.alertTimer('Email không hợp lệ !', 'warning', 2000);
		}
	};

	useEffect(() => {
		const wowContact = new WOW({
			boxClass: 'wowContact',
			live: false,
			// mobile: true,
		});
		wowContact.init();
	}, []);
	return (
		<Box className={`wowContact fadeInUp ${classes.boxCompContact}`}>
			<Box className={classes.colorCoverContact}></Box>

			<Container maxWidth="lg" className={classes.containerPadding}>
				<Box display="flex" style={{ height: 550 }}>
					<Grid container alignItems="center">
						{/* Input Email */}
						<Grid container item md={6} className={classes.gridInputEmail}>
							<Box width="100%" className={classes.boxInputEmail}>
								<Typography
									variant="h6"
									contained="h6"
									style={{ marginBottom: 10 }}
									color="primary"
									className={classes.titleContact}
								>
									ĐĂNG KÝ NHẬN THÔNG TIN CẬP NHẬT VÀ ƯU ĐÃI QUA EMAIL
								</Typography>
								<Typography style={{ color: '#fff', marginBottom: 15 }}>
									Cơ hội nhận được mã giảm giá và thông tin phim mới nhanh nhất.
								</Typography>
								<Formik initialValues={{ email: '' }} onSubmit={handleSignUpEmail}>
									{(formikProps) => (
										<Form>
											<TextField
												InputProps={{
													className: classes.styleTextField,
													disableUnderline: true,
												}}
												placeholder="Nhập thông tin Email..."
												name="email"
												onChange={formikProps.handleChange}
											></TextField>
											<Button classes={{ root: classes.btnEmail }} type="submit">
												Xác nhận
												<ArrowForwardRoundedIcon className={classes.btnAccept} />
											</Button>
										</Form>
									)}
								</Formik>
							</Box>
						</Grid>
						{/* Mobile Application */}
						<Grid container item md={6} className={classes.gridMobileApp}>
							<Box display="flex" alignItems="center" width="100%">
								<Box className={classes.boxSlider}>
									<Slider {...settings} className={classes.styleSliderSetting}>
										<Box>
											<LazyLoad once={true}>
												<img src={slide2} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide3} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide4} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide5} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide6} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide7} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide8} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide9} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide10} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide11} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide12} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide13} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide14} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
										<Box>
											<LazyLoad once={true}>
												<img src={slide15} alt="slider" className={classes.imgSlider} />
											</LazyLoad>
										</Box>
									</Slider>
									<LazyLoad once={true}>
										<img
											src={iphoneXScreen}
											alt="display-application"
											style={{
												position: 'absolute',
												top: 0,
												left: 0,
												bottom: 0,
												right: 0,
												width: '100%',
												height: '100%',
											}}
										/>
									</LazyLoad>
								</Box>
								<Box>
									<Typography
										variant="h6"
										contained="h6"
										style={{ marginBottom: 10 }}
										color="primary"
										className={classes.titleContact}
									>
										ỨNG DỤNG ĐẶT VÉ XEM PHIM TIỆN LỢI ĐÃ CÓ TRÊN SMARTPHONE
									</Typography>
									<Typography className={classes.descDownloadMobileApp}>
										Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp
										dẫn.
									</Typography>
									<Typography style={{ color: '#fff', marginBottom: 10 }}>
										Tải ứng dụng ngay:
									</Typography>
									<Link to={{ pathname: 'https://www.apple.com/app-store/' }} target="_blank">
										<LazyLoad once={true}>
											<img
												src={ios}
												alt="ios-phone"
												style={{ marginRight: 10 }}
												className={classes.styleImgLogoOS}
											/>
										</LazyLoad>
									</Link>
									<Link to={{ pathname: 'https://play.google.com/store' }} target="_blank">
										<LazyLoad once={true}>
											<img src={android} alt="android-phone" className={classes.styleImgLogoOS} />
										</LazyLoad>
									</Link>
								</Box>
							</Box>
						</Grid>
					</Grid>
				</Box>
			</Container>
		</Box>
	);
};
export default withStyles(styles)(React.memo(Contact));
