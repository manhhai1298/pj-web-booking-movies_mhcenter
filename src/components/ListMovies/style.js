import BackgroundMovie from '../../assets/image/background/bgListMovies.jpg';
const styles = (theme) => {
	return {
		colorCoverListMovies: {},
		containerListMovies: {
			zIndex: 1,
			position: 'relative',
			backgroundImage: `url(${BackgroundMovie})`,
			backgroundSize: '100% 100%',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundAttachment: 'fixed',
			backgroundColor: 'black',
			textAlign: 'center',
			'& $colorCoverListMovies': {
				position: 'absolute',
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				zIndex: -2,
				backgroundColor: 'rgb(0,0,0,0.9)',
			},
		},
		btnViewMore: {
			textAlign: 'center',
			color: 'black',
			borderRadius: 20,
			width: 130,
		},

		btnGroup: {
			margin: '20px 0',
		},
		boxMovie: { padding: '0 15px' },

		[theme.breakpoints.down('md')]: { titleShowPage: { fontSize: 27 } },
		[theme.breakpoints.down('xs')]: { titleShowPage: { fontSize: 20 } },
	};
};
export default styles;
