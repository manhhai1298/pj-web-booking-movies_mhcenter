import { Box, Button, ButtonGroup, Container, Grid, Typography, withStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import ExpandMoreRoundedIcon from '@material-ui/icons/ExpandMoreRounded';
import { useSelector } from 'react-redux';
import Movie from '../Movie/Movie';
import styles from './style';
import { WOW } from 'wowjs';
import LoadFetch from '../CompLoading/LoadComponent/LoadFetch';
const ListMovies = ({ classes, onRef }) => {
	const [widthScreen, setWidthScreen] = useState(window.innerWidth);
	const [styleImage, setStyleImage] = useState({});

	const [isShowPage, setIsShowPage] = useState(true);
	const [isAmountShow, setIsAmountShow] = useState(0);
	const [isAmountShowReverse, setIsAmountShowReverse] = useState(0);
	const handleClickButton = (inputShowPage) => {
		if (inputShowPage !== isShowPage) setIsShowPage(inputShowPage);
	};

	const handleShowMore = () => {
		isShowPage ? setIsAmountShowReverse(isAmountShowReverse + 4) : setIsAmountShow(isAmountShow + 4);
	};

	//get List Movies from Redux
	const listMovies = useSelector((state) => {
		return state.movie.listMovies;
	});
	// listMovies.length - 4 * isAmountShow
	let listMoviesShow = listMovies.slice(0, isAmountShow + 4);

	const initialWowListMovie = () => {
		const wowListMovie = new WOW({
			boxClass: 'wowListMovie',
			live: false,
			// mobile: true,
		});
		wowListMovie.init();
	};

	let listMoviesShowReverse = listMovies
		.slice(0)
		.reverse()
		.slice(0, isAmountShowReverse + 4);
	useEffect(() => {
		initialWowListMovie();

		if (widthScreen <= 600) setStyleImage({ height: '50vw', width: '37vw', borderRadius: '7px' });
		else if (widthScreen > 600 && widthScreen <= 960)
			setStyleImage({ height: '40vw', width: '27vw', borderRadius: '7px' });
		else if (widthScreen > 960 && widthScreen <= 1280)
			setStyleImage({ height: '32vw', width: '21vw', borderRadius: '7px' });
		else if (widthScreen > 1280 && widthScreen <= 1650)
			setStyleImage({ height: '22vw', width: '16vw', borderRadius: '7px' });
		else if (widthScreen > 1650 && widthScreen <= 2000)
			setStyleImage({ height: '20vw', width: '14vw', borderRadius: '7px' });
		else if (widthScreen > 2000) setStyleImage({ height: '17vw', width: '12vw', borderRadius: '7px' });

		const handleResize = () => {
			setWidthScreen(window.innerWidth);
		};

		window.addEventListener('resize', handleResize);

		handleResize();
	}, [widthScreen]);

	return listMovies.length > 0 ? (
		<Box name="ListMovies" className={`wowListMovie fadeInUp ${classes.containerListMovies}`}>
			<Container>
				<Box className={classes.colorCoverListMovies}></Box>
				<ButtonGroup
					variant="text"
					color="primary"
					aria-label="text primary button group"
					className={classes.btnGroup}
				>
					<Button style={{ borderRight: '2px solid' }} onClick={() => handleClickButton(true)}>
						<Typography
							variant="h4"
							contained="h4"
							style={isShowPage ? { borderBottom: '2px solid ' } : {}}
							className={`wowListMovie fadeInLeft ${classes.titleShowPage}`}
						>
							ĐANG CHIẾU
						</Typography>
					</Button>
					<Button onClick={() => handleClickButton(false)}>
						<Typography
							variant="h4"
							contained="h4"
							style={isShowPage ? {} : { borderBottom: '2px solid ' }}
							className={`wowListMovie fadeInRight ${classes.titleShowPage}`}
						>
							SẮP CHIẾU
						</Typography>
					</Button>
				</ButtonGroup>
				{isShowPage ? (
					<Grid container>
						{listMoviesShowReverse.map((movie) => (
							<Grid
								container
								key={movie.maPhim}
								item
								md={3}
								sm={4}
								xs={6}
								className={classes.itemGridMovie}
								justify="center"
							>
								<Box className={classes.boxMovie}>
									<Movie movie={movie} colorText={'#fff'} imgMovie={styleImage} />
								</Box>
							</Grid>
						))}
					</Grid>
				) : (
					<Grid container>
						{listMoviesShow.map((movie) => (
							<Grid
								container
								key={movie.maPhim}
								item
								md={3}
								sm={4}
								xs={6}
								className={classes.itemGridMovie}
								justify="center"
							>
								<Box className={classes.boxMovie}>
									<Movie movie={movie} colorText={'#fff'} imgMovie={styleImage} />
								</Box>
							</Grid>
						))}
					</Grid>
				)}
			</Container>
			<Button
				variant="contained"
				color="primary"
				classes={{ root: classes.btnViewMore }}
				style={{
					margin: 10,
					backgroundColor: 'black',
					color: '#fdb73b',
					border: '2px solid #fdb73b',
				}}
				onClick={handleShowMore}
			>
				Xem thêm
				<ExpandMoreRoundedIcon />
			</Button>
		</Box>
	) : (
		<LoadFetch />
	);
};

export default withStyles(styles)(React.memo(ListMovies));
