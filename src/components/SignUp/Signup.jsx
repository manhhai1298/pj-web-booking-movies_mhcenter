import { Box, Button, Grid, IconButton, TextField, Typography, withStyles } from '@material-ui/core';
import { ErrorMessage, Form, Formik } from 'formik';
import React, { forwardRef } from 'react';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import * as yup from 'yup';
import styles from './style';
import { swAlertService, userService } from '../../Services/initServices';

const Signup = forwardRef(({ classes, closeSignUpModal }, ref) => {
	const handleSubmitSignUp = (value) => {
		userService
			.signUp(value)
			.then((res) => {
				closeSignUpModal();
				swAlertService.alertTimer('Đăng ký tài khoản thành công', 'success', 2000);
			})
			.catch((err) => {
				swAlertService.alertTimer(err.response.data, 'error', 2000);
			});
	};
	const signUpObjectValidate = yup.object().shape({
		taiKhoan: yup.string().required('* Bắt buộc nhập').min(3, '* Ít nhất 3 ký tự'),
		matKhau: yup.string().required('* Bắt buộc nhập').min(3, '* Ít nhất 3 ký tự'),
		hoTen: yup
			.string()
			.required('* Bắt buộc nhập')
			.matches(/\b[A-Za-z]+[ ]+[A-Za-z]+\b/, ' *Nhập đầy đủ họ tên'),
		email: yup.string().required('* Bắt buộc nhập').email('* Không đúng định dạng'),
		soDt: yup
			.string()
			.required('* Bắt buộc nhập')
			.matches(/^[0-9]+$/, '* Không đúng định dạng'),
	});
	return (
		<div className={classes.compSignUp}>
			<Typography variant="h4" contained="h4" className={classes.titleSignUp}>
				Đăng ký tài khoản
			</Typography>
			<Formik
				initialValues={{
					taiKhoan: '',
					matKhau: '',
					hoTen: '',
					email: '',
					soDt: '',
					maLoaiNguoiDung: 'KhachHang',
					maNhom: 'GP09',
				}}
				onSubmit={handleSubmitSignUp}
				validationSchema={signUpObjectValidate}
			>
				{(formikProps) => (
					<Form>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item>
								<TextField
									label="Tài khoản"
									className={classes.styleTextField}
									name="taiKhoan"
									onChange={formikProps.handleChange}
								/>
							</Grid>
							<Box display="block">
								<ErrorMessage name="taiKhoan" component="div" className={classes.styleErrorMessage} />
							</Box>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item>
								<TextField
									label="Mật khẩu"
									type="password"
									className={classes.styleTextField}
									name="matKhau"
									onChange={formikProps.handleChange}
								/>
							</Grid>
							<Box display="block">
								<ErrorMessage name="matKhau" component="div" className={classes.styleErrorMessage} />
							</Box>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item>
								<TextField
									label="Họ tên"
									className={classes.styleTextField}
									name="hoTen"
									onChange={formikProps.handleChange}
								/>
							</Grid>
							<Box display="block">
								<ErrorMessage name="hoTen" component="div" className={classes.styleErrorMessage} />
							</Box>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item>
								<TextField
									label="Địa chỉ email"
									className={classes.styleTextField}
									name="email"
									onChange={formikProps.handleChange}
								/>
							</Grid>
							<Box display="block">
								<ErrorMessage name="email" component="div" className={classes.styleErrorMessage} />
							</Box>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end">
							<Grid item>
								<TextField
									label="Số điện thoại"
									className={classes.styleTextField}
									name="soDt"
									onChange={formikProps.handleChange}
								/>
							</Grid>
							<Box display="block">
								<ErrorMessage name="soDt" component="div" className={classes.styleErrorMessage} />
							</Box>
						</Grid>
						<Grid container spacing={1} alignItems="flex-end" justify="center">
							<Grid item>
								<Button
									type="submit"
									variant="contained"
									color="primary"
									style={{ margin: '15px 0 10px 0' }}
								>
									Đăng ký
								</Button>
							</Grid>
						</Grid>
					</Form>
				)}
			</Formik>

			<IconButton className={classes.btnCloseModal} onClick={closeSignUpModal}>
				<HighlightOffIcon />
			</IconButton>
		</div>
	);
});
export default withStyles(styles)(React.memo(Signup));
