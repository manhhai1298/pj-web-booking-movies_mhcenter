const styles = (theme) => {
	return {
		compSignUp: {
			backgroundColor: '#fff',
			padding: '40px 50px 15px 50px',
			borderRadius: 15,
			textAlign: 'center',
			position: 'relative',
		},
		styleTextField: {
			width: '25vw',
			margin: '5px 0',
		},
		btnCloseModal: {
			position: 'absolute',
			top: 0,
			right: 0,
			color: 'black',
		},
		styleErrorMessage: {
			color: 'red',
		},
		[theme.breakpoints.down('md')]: {},
		[theme.breakpoints.down('sm')]: {
			compSignUp: {
				padding: 20,
			},
			styleTextField: {
				width: '60vw',
				margin: '2px 0',
			},
			titleSignUp: {
				marginTop: 20,
				fontSize: 26,
			},
		},
		[theme.breakpoints.down('xs')]: {
			compSignUp: {
				padding: 10,
			},
			styleTextField: {
				width: '80vw',
				margin: '2px 0',
			},
			titleSignUp: {
				marginTop: 20,
				fontSize: 22,
			},
		},
	};
};
export default styles;
