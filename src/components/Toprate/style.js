const styles = (theme) => {
	return {
		titleTopMovies: {
			margin: '30px 0 10px 0',
		},
		styleSlider: {
			'& div.slick-list': {
				padding: '10px 0',
			},
			'& .MuiBox-root': { outline: 'none' },
		},
		prevBtnSlick: {
			position: 'absolute',
			padding: 2,
			zIndex: 12,
			left: -11,
			top: '33%',
			backgroundColor: theme.palette.primary.main,
			fontSize: 32,
			borderRadius: '50%',
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
				cursor: 'pointer',
				animation: 'shake 5s linear both',
				animationIterationCount: 'infinite',
			},
		},
		nextBtnSlick: {
			position: 'absolute',
			padding: 4,
			right: -15,
			top: '33%',
			backgroundColor: theme.palette.primary.main,
			fontSize: 30,
			borderRadius: '50%',
			'&:hover': {
				backgroundColor: theme.palette.primary.dark,
				cursor: 'pointer',
				animation: 'shake 5s linear both',
				animationIterationCount: 'infinite',
			},
		},

		[theme.breakpoints.down('md')]: {
			prevBtnSlick: { top: '35%' },
			nextBtnSlick: { top: '35%' },
			// ribbon: {
			// 	width: '20%',
			// },
		},
		[theme.breakpoints.down('sm')]: {
			prevBtnSlick: { padding: '2', fontSize: 20, top: '29%' },
			nextBtnSlick: { padding: '2', fontSize: 20, top: '29%' },
			// ribbon: {
			// 	width: '30%',
			// },
		},
		[theme.breakpoints.down('xs')]: {
			prevBtnSlick: {
				padding: '1',
				fontSize: 18,
				top: '27%',
			},
			nextBtnSlick: {
				padding: '1',
				fontSize: 18,
				top: '27%',
			},
			// ribbon: {
			// 	width: '65%',
			// },
		},
	};
};
export default styles;
