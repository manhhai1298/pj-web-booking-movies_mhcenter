import { Box, Container, Typography, withStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styles from './style';
import Movie from '../Movie/Movie';
import ArrowBackIosRoundedIcon from '@material-ui/icons/ArrowBackIosRounded';
import ArrowForwardIosRoundedIcon from '@material-ui/icons/ArrowForwardIosRounded';
import { useDispatch, useSelector } from 'react-redux';
import { createAction } from '../../redux/actions/actionCreator';
import { GET_LIST_MOVIES } from '../../redux/actions/type';
import { movieService } from '../../Services/initServices';
import { WOW } from 'wowjs';
import LoadFetch from '../CompLoading/LoadComponent/LoadFetch';
const Toprate = ({ classes }) => {
	//Customize ArrowButton Slick
	const SampleNextArrow = (props) => {
		return <ArrowForwardIosRoundedIcon className={classes.nextBtnSlick} onClick={props.onClick} />;
	};

	const SamplePrevArrow = (props) => {
		return <ArrowBackIosRoundedIcon className={classes.prevBtnSlick} onClick={props.onClick} />;
	};
	const settings2 = {
		autoplay: true,
		autoplaySpeed: 3000,
		dots: false,
		infinite: true,
		speed: 900,
		slidesToShow: 5,
		slidesToScroll: 1,
		nextArrow: <SampleNextArrow />,
		prevArrow: <SamplePrevArrow />,
		responsive: [
			{
				breakpoint: 1280,
				settings: {
					slidesToShow: 4,
				},
			},
			{
				breakpoint: 960,
				settings: {
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
				},
			},
		],
	};

	//init Hook
	const dispatch = useDispatch();

	const initialWowTopRate = () => {
		const wowTopRate = new WOW({
			boxClass: 'wowTopRate',
			live: false,
			// mobile: true,
		});
		wowTopRate.init();
	};

	//CALL API
	useEffect(() => {
		initialWowTopRate();
		movieService
			.getListMovies()
			.then((res) => {
				dispatch(createAction(GET_LIST_MOVIES, res.data));
			})
			.catch();
	}, [dispatch]);
	//get List Movies from Redux
	const listMovies = useSelector((state) => state.movie.listMovies);
	return listMovies.length > 0 ? (
		<Container maxWidth="lg" style={{ marginBottom: 5 }}>
			{/* TITLE TOP MOVIES */}
			<Typography
				variant="h4"
				contained="h4"
				className={`wowTopRate fadeInLeft ${classes.titleTopMovies}`}
				data-wow-delay="2.1s"
			>
				PHIM HOT
			</Typography>

			{/* AREA SLICK CONTAIN LIST TOP MOVIES */}
			<Slider {...settings2} className={`wowTopRate fadeInUp ${classes.styleSlider}`}>
				{listMovies
					.filter((movie) => movie.danhGia >= 4)
					.slice(0, 9)
					.map((movie) => (
						<Box key={movie.maPhim}>
							<Movie movie={movie} colorText={'black'} />
						</Box>
					))}
			</Slider>
		</Container>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(Toprate));
