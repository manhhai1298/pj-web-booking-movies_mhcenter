import { createMuiTheme } from '@material-ui/core';
const theme = createMuiTheme({
	typography: {
		fontFamily: "'Varela Round', sans-serif",
		fontSize: 13,
		fontWeightLight: 400,
		fontWeightRegular: 400,
		fontWeightMedium: 500,
		fontWeightBold: 600,
		button: {
			fontFamily: "'Varela Round', sans-serif",
			fontSize: 13,
			fontWeight: 600,
		},
	},
	palette: {
		primary: {
			light: '#ffc359',
			main: '#fdb73b',
			dark: '#ffa300',
		},
	},
	spacing: 10,
});
export default theme;
