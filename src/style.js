const styles = (theme) => {
	return {
		globalStyle: {
			padding: theme.spacing(0),
			margin: theme.spacing(0),
		},
	};
};

export default styles;
