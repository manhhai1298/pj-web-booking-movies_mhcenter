import { SET_TOKEN_USER } from '../actions/type';

let initialState = {
	rdInfoUser: {
		accessToken: '',
		taiKhoan: '',
		hoTen: '',
	},
};

const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case SET_TOKEN_USER:
			state.rdInfoUser = { ...payload };
			return { ...state };
		default:
			return state;
	}
};
export default reducer;
