import { CLEAR_LIST_CHOICED, POST_LIST_SEATS, POST_SEAT_CHOICED, CLEAR_LIST_CHOICED_FIRST } from '../actions/type';

let initialState = {
	listSeats: [],
	listSeatsChoiced: [],
};

const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case POST_LIST_SEATS:
			state.listSeats = payload;
			return { ...state };
		case POST_SEAT_CHOICED:
			const listTemp = [...state.listSeatsChoiced];
			const index = listTemp.findIndex((seat) => seat.maGhe === payload.maGhe);
			if (index === -1) listTemp.push(payload);
			else listTemp.splice(index, 1);
			state.listSeatsChoiced = listTemp;
			return { ...state };
		case CLEAR_LIST_CHOICED:
			const listSeatsTemp = [...state.listSeats];
			for (let seat1 of listSeatsTemp) {
				for (let seat2 of state.listSeatsChoiced) {
					if (seat1.maGhe === seat2.maGhe) {
						seat1.daDat = true;
					}
				}
			}
			state.listSeats = listSeatsTemp;
			state.listSeatsChoiced = [];
			return { ...state };
		case CLEAR_LIST_CHOICED_FIRST:
			state.listSeatsChoiced = [];
			return { ...state };
		default:
			return state;
	}
};
export default reducer;
