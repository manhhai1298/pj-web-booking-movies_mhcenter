import { GET_LIST_MOVIES } from '../actions/type';

const typeMovie = [
	'Hành động',
	'Phiêu lưu',
	'Viễn tưởng',
	'Kịch tính',
	'Tâm lý',
	'Tình cảm',
	'Khoa học',
	'Chiến tranh',
	'Hài kịch',
	'Gia đình',
	'Lãng mạn',
	'Giật gân',
	'Kinh dị',
];
const handleRandomTypeMovie = () => {
	let typesMovie = '';
	let count = 1;
	while (count < 3) {
		const typeRandom = typeMovie[Math.floor(Math.random() * typeMovie.length)];
		if (typesMovie.indexOf(typeRandom) === -1) {
			typesMovie += typeRandom + ', ';
			count++;
		}
	}
	return typesMovie.slice(0, typesMovie.length - 2);
};
let initialState = { listMovies: [] };

const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case GET_LIST_MOVIES:
			state.listMovies = payload.map((movie) => {
				return { ...movie, typeMovie: handleRandomTypeMovie() };
			});
			return { ...state };
		default:
			return state;
	}
};
export default reducer;
