import {
	GET_LIST_MOVIES_IN_THEATER,
	GET_LIST_THEATERGROUP,
	GET_LIST_THEATER_BY_ID,
	POST_ID_THEATER_CHOOSE,
	POST_THEATERGROUP_CHOOSED,
} from '../actions/type';

let initialState = {
	listTheaterGroups: [],
	theaterGroupChoosed: {
		maHeThongRap: 'BHDStar',
		logo: 'http://movie0706.cybersoft.edu.vn/hinhanh/bhd-star-cineplex.png',
	},
	listTheatersByIdGroup: [],
	idTheaterChoose: 'bhd-star-cineplex-3-2',
	listMoviesInTheater: [],
};

const reducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case GET_LIST_THEATERGROUP:
			state.listTheaterGroups = payload;
			return { ...state };
		case POST_THEATERGROUP_CHOOSED:
			state.theaterGroupChoosed = payload;
			return { ...state };
		case GET_LIST_THEATER_BY_ID:
			state.listTheatersByIdGroup = payload;
			return { ...state };
		case POST_ID_THEATER_CHOOSE:
			state.idTheaterChoose = payload;
			return { ...state };
		case GET_LIST_MOVIES_IN_THEATER:
			state.listMoviesInTheater = payload;
			return { ...state };
		default:
			return state;
	}
};

export default reducer;
