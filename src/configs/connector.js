import axios from 'axios';

const createConnector = (customConfig) => {
	const config = {};

	const token = JSON.parse(localStorage.getItem('infoUser')).accessToken;

	if (token) {
		config.headers = {
			Authorization: 'Bearer ' + token,
		};
	}

	return axios({ ...customConfig, ...config });
};

export default createConnector;
