import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import movie from '../redux/reducers/movie';
import theater from '../redux/reducers/theater';
import user from '../redux/reducers/user';
import seat from '../redux/reducers/seat';
//create Root Reducer
const reducer = combineReducers({
	movie,
	theater,
	user,
	seat,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
