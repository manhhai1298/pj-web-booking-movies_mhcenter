import { Box, Button, Container, Grid, Typography, withStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import styles from './style';
import { Rating } from '@material-ui/lab';
import PlayCircleOutlineRoundedIcon from '@material-ui/icons/PlayCircleOutlineRounded';
import MovieCreationRoundedIcon from '@material-ui/icons/MovieCreationRounded';
import { useRouteMatch } from 'react-router-dom';
import { theaterService } from '../../Services/initServices';
import { Link as LinkScroll } from 'react-scroll';
import Swal from 'sweetalert2';
import ScheduleDetailMovie from '../../components/ScheduleDetailMovie/ScheduleDetailMovie';
import LazyLoad from 'react-lazyload';
import LoadFetch from '../../components/CompLoading/LoadComponent/LoadFetch';
const listdDirectors = [
	'Steven Spielberg',
	'Joe Russo, Anthony Russo',
	'Michael Bay',
	'James Cameron',
	'Christopher Nolan',
	'Peter Jackson',
	'David Yates',
	'Tim Burton',
	'Ron Howard',
	'Roland Emmerich',
	'Pierre Coffin',
	'George Lucas',
	'James Wan',
];

const typeMovie = [
	'Hành động',
	'Phiêu lưu',
	'Viễn tưởng',
	'Kịch tính',
	'Tâm lý',
	'Tình cảm',
	'Khoa học',
	'Chiến tranh',
	'Hài kịch',
	'Gia đình',
	'Lãng mạn',
	'Giật gân',
	'Kinh dị',
];

const handleRandomTypeMovie = () => {
	let typesMovie = '';
	let count = 1;
	while (count < 4) {
		const typeRandom = typeMovie[Math.floor(Math.random() * typeMovie.length)];
		if (typesMovie.indexOf(typeRandom) === -1) {
			typesMovie += typeRandom + ', ';
			count++;
		}
	}
	return typesMovie.slice(0, typesMovie.length - 2);
};

const Detail = ({ classes }) => {
	const handleWidthTrailer = () => {
		const widthScreen = window.innerWidth;
		if (widthScreen <= 600) return `width="280" height="220"`;
		else if (widthScreen > 600 && widthScreen <= 960) return `width="540" height="380"`;
		else if (widthScreen > 960 && widthScreen <= 1280) return `width="864" height="480"`;
		else if (widthScreen > 1280 && widthScreen <= 1920) return `width="1024" height="576"`;
		else if (widthScreen > 1920) return `width="1366" height="768"`;
		return `width="854" height="480"`;
	};

	//View Trailer Movie
	const handleViewTrailer = (url) => {
		Swal.fire({
			showCancelButton: true,
			cancelButtonText: 'Đóng',
			cancelButtonColor: '#d33',
			showConfirmButton: false,
			customClass: classes.styleAlertTrailer,
			html: `<iframe ${handleWidthTrailer()} src="${url}?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`,
		});
	};
	const route = useRouteMatch();
	const [isLoaded, setIsLoaded] = useState(false);
	const [infoShowMovie, setInfoShowMovie] = useState({});
	useEffect(() => {
		theaterService
			.getInfoShowMovie(route.params.movieId)
			.then((res) => {
				setInfoShowMovie(res.data);
				setIsLoaded(true);
			})
			.catch();
	}, [route]);
	return isLoaded ? (
		<Box className={classes.compDetail}>
			<Container className={classes.containerCompDetail} maxWidth="md">
				<Box className={classes.colorCover}></Box>
				<Box pb={3}>
					<Grid container>
						{/* Left Content */}
						<Grid item sm={4} xs={12}>
							<Box style={{ textAlign: 'center' }}>
								<LazyLoad once={true}>
									<img
										src={
											infoShowMovie.hinhAnh.indexOf('https') === -1
												? infoShowMovie.hinhAnh.replace('http', 'https')
												: null
										}
										alt="img-detail-movie"
										className={classes.imgDetailMovie}
									/>
								</LazyLoad>
								<Box>
									<LinkScroll to="scheduleMovie" spy={true} smooth={true} duration={1000}>
										<Button
											variant="contained"
											color="primary"
											classes={{ root: classes.styleBtnDetailMovie }}
											className="wow fadeInLeft"
										>
											<MovieCreationRoundedIcon />
											Booking
										</Button>
									</LinkScroll>
									<Button
										variant="contained"
										color="primary"
										classes={{ root: classes.styleBtnDetailMovie }}
										className="wow fadeInRight"
										onClick={() => handleViewTrailer(infoShowMovie.trailer)}
									>
										<PlayCircleOutlineRoundedIcon />
										Trailer
									</Button>
								</Box>
							</Box>
						</Grid>
						{/* Right Content */}
						<Grid item sm={8} xs={12}>
							<Box justifyContent="flex-start" className={classes.boxTextMovie}>
								<Typography
									variant="h3"
									container="h3"
									className={(classes.textDetailItem, classes.textTitleItem)}
								>
									{infoShowMovie.tenPhim}
								</Typography>
								<Box className={classes.boxDigital}>
									<Box
										color="primary"
										style={{
											border: '2px solid #fdb73b',
											borderRadius: 4,
											marginTop: 5,
											fontSize: 12,
											fontWeight: 'bold',
											padding: '0 5px',
											display: 'inline-block',
										}}
										className={classes.textDetailItem}
									>
										<Typography variant="h6" color="primary" style={{ fontSize: 16 }}>
											2D Digital
										</Typography>
									</Box>
								</Box>

								<Typography className={classes.textDetailItem}>
									<b style={{ color: ' #fdb73b' }}>Nội dung phim:</b> {infoShowMovie.moTa}
								</Typography>
								<Typography className={classes.textDetailItem}>
									<b style={{ color: ' #fdb73b' }}>Đạo diễn:</b>{' '}
									{listdDirectors[Math.floor(Math.random() * listdDirectors.length)]}
								</Typography>
								<Typography className={classes.textDetailItem}>
									<b style={{ color: ' #fdb73b' }}>Thể loại:</b> {handleRandomTypeMovie()}
								</Typography>
								<Typography className={classes.textDetailItem}>
									<b style={{ color: ' #fdb73b' }}>Ngày khởi chiếu:</b>{' '}
									{infoShowMovie.ngayKhoiChieu.substring(8, 10) +
										infoShowMovie.ngayKhoiChieu.substring(4, 8) +
										infoShowMovie.ngayKhoiChieu.substring(0, 4)}
								</Typography>
								<Typography className={classes.textDetailItem}>
									<b style={{ color: ' #fdb73b' }}>Thời lượng:</b>{' '}
									{Math.floor(Math.random() * 60) + 90} phút
								</Typography>
								<Typography className={classes.textDetailItem}>
									<b style={{ color: ' #fdb73b' }}>Ngôn ngữ:</b> Phụ đề tiếng Việt
								</Typography>
								<Box display="flex" alignItems="center" className={classes.textDetailItem}>
									<Typography>
										<b style={{ color: ' #fdb73b' }}>Đánh giá:</b>
									</Typography>
									<Rating
										name="hover-feedback"
										value={infoShowMovie.danhGia}
										precision={0.5}
										style={{ margin: '0 5px' }}
										readOnly
									/>
								</Box>
							</Box>
						</Grid>
					</Grid>
				</Box>
				<ScheduleDetailMovie infoShowMovie={infoShowMovie} />
			</Container>
		</Box>
	) : (
		<LoadFetch />
	);
};
export default withStyles(styles)(React.memo(Detail));
