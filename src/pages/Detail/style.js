import BackgroundMovie from '../../assets/image/background/bgListMovies.jpg';
const styles = (theme) => {
	return {
		colorCover: {},
		compDetail: {
			color: '#fff',
			zIndex: 1,
			position: 'relative',
			backgroundImage: `url(${BackgroundMovie})`,
			backgroundSize: '100% 100%',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundAttachment: 'fixed',
			backgroundColor: 'black',
			padding: '80px 0',
			'& $colorCover': {
				position: 'absolute',
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				zIndex: -2,
				backgroundColor: 'rgb(0,0,0,0.9)',
			},
		},
		imgDetailMovie: {
			borderRadius: 7,
		},
		textDetailItem: {
			marginBottom: 20,
		},
		styleBtnDetailMovie: {
			color: 'black',
			borderRadius: 5,
			width: 100,
			margin: '10px 5px',
		},
		styleAlertTrailer: {
			padding: theme.spacing(1),
			width: 'auto',
			height: 'auto',
			'& .swal2-content': {
				padding: 0,
			},
		},
		boxTextMovie: {
			marginLeft: 40,
		},
		[theme.breakpoints.down('xl')]: {
			imgDetailMovie: {
				width: '13vw',
				height: '19vw',
			},
		},
		[theme.breakpoints.down('lg')]: {
			imgDetailMovie: {
				width: '17vw',
				height: '24vw',
			},
		},
		[theme.breakpoints.down('md')]: {
			imgDetailMovie: {
				width: '24vw',
				height: '33vw',
			},
		},
		[theme.breakpoints.down('sm')]: {
			imgDetailMovie: {
				width: '30vw',
				height: '43vw',
			},
		},
		[theme.breakpoints.down('xs')]: {
			compDetail: { padding: '40px 0' },
			containerCompDetail: { padding: '0 20px' },
			imgDetailMovie: {
				width: '68vw',
				height: '88vw',
			},
			boxTextMovie: { padding: '0 5px', margin: 0 },
			textTitleItem: {
				textAlign: 'center',
				fontSize: 24,
			},
			boxDigital: {
				textAlign: 'center',
			},
		},
	};
};
export default styles;
