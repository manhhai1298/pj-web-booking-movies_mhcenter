import React from 'react';
import Carousel from '../../components/Carousel/Carousel';
import Contact from '../../components/Contact/Contact';
import ListMovies from '../../components/ListMovies/ListMovies';
import Theatergroup from '../../components/Theatergroup/Theatergroup';
import Toprate from '../../components/Toprate/Toprate';
import ListTheaterLayout from '../../HOCs/ListTheaterLayout';
const TheaterGroup = ListTheaterLayout(Theatergroup);
const Home = () => {
	return (
		<div style={{ maxWidth: '100vw', width: '100%', overflow: 'hidden' }}>
			<Carousel />
			<Toprate />
			<ListMovies />
			<TheaterGroup />
			<Contact />
		</div>
	);
};
export default React.memo(Home);
