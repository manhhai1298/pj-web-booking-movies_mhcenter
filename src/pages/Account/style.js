import BackgroundMovie from '../../assets/image/background/bgListMovies.jpg';

const styles = (theme) => {
	return {
		colorCover: {},
		compInfoAcc: {
			color: 'black',
			zIndex: 1,
			position: 'relative',
			backgroundImage: `url(${BackgroundMovie})`,
			backgroundSize: '100% 100%',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundAttachment: 'fixed',
			backgroundColor: 'black',
			'& $colorCover': {
				position: 'absolute',
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				zIndex: -2,
				backgroundColor: 'rgb(0,0,0,0.9)',
			},
			'& .MuiTabs-indicator': { backgroundColor: '#ffa300' },
		},
		boxContent: {
			display: 'flex',
		},
		tabs: {
			width: '15vw',
		},
		tabsMobile: {
			display: 'none',
		},
		boxCover: {
			backgroundColor: 'white',
			borderRadius: 7,
			padding: '20px 0',
			height: 550,
		},
		contentChange: {
			display: 'flex',
			justifyContent: 'center',
			alignContent: 'center',
			width: '100%',
			padding: '0 20px',
		},
		[theme.breakpoints.down('md')]: {
			tabs: {
				width: '20vw',
			},
		},
		[theme.breakpoints.down('sm')]: {
			boxContent: {
				display: 'inline-block',
				margin: '0 auto',
				textAlign: 'center',
			},
			tabs: { display: 'none' },
			tabsMobile: { display: 'block', width: 'auto', marginBottom: 50 },
			boxCover: { display: 'flex', height: 650 },
			contentChange: {
				display: 'flex',
				justifyContent: 'center',
				alignContent: 'center',
				width: '100%',
				padding: '0',
			},
		},
		[theme.breakpoints.down('xs')]: {
			tabs: { display: 'block', width: 'auto', marginBottom: 50 },
			tabsMobile: { display: 'none' },
			boxCover: {
				height: 750,
			},
		},
	};
};
export default styles;
