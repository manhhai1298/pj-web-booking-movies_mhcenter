import { Box, Container, Tab, Tabs, withStyles } from '@material-ui/core';
import React, { useEffect } from 'react';
import styles from './style';
import { userService } from '../../Services/initServices';
import { useState } from 'react';
import Infoaccount from '../../components/ComponentAccount/InfoAccount/Infoaccount';
import Listbookedaccount from '../../components/ComponentAccount/ListBookedAccount/Listbookedaccount';
import Changepassword from '../../components/ComponentAccount/ChangePassword/Changepassword';
import { Redirect, useRouteMatch } from 'react-router-dom';
import LoadComponent from '../../components/CompLoading/LoadComponent/LoadComponent';

const Account = ({ classes }) => {
	//config Tabs
	const route = useRouteMatch();
	const [selectedTab, setSelectedTab] = useState(+route.params.positionTab);
	const userSignIn = JSON.parse(localStorage.getItem('infoUser'));
	const handleChange = (event, newValue) => {
		setSelectedTab(newValue);
	};
	//config Tabs

	//init hook
	const [infoAccount, setInfoAccount] = useState(null);

	useEffect(() => {
		const account = {
			taiKhoan: JSON.parse(localStorage.getItem('infoUser')).taiKhoan,
		};
		userService
			.getInfoAccount(account)
			.then((res) => {
				setInfoAccount(res.data);
			})
			.catch();
	}, [route]);
	return userSignIn.accessToken ? (
		infoAccount ? (
			<Box className={classes.compInfoAcc} py={8}>
				<Container maxWidth="lg">
					<Box className={classes.colorCover}></Box>
					<Box className={classes.boxCover}>
						<Box className={classes.boxContent}>
							<Tabs
								orientation="vertical"
								variant="scrollable"
								value={selectedTab}
								onChange={handleChange}
								className={classes.tabs}
							>
								<Tab label="Thông tin tài khoản" />
								<Tab label="Đổi mật khẩu" />
								<Tab label="Danh sách đặt vé" />
							</Tabs>
							<Tabs
								variant="scrollable"
								value={selectedTab}
								onChange={handleChange}
								className={classes.tabsMobile}
							>
								<Tab label="Thông tin tài khoản" />
								<Tab label="Đổi mật khẩu" />
								<Tab label="Danh sách đặt vé" />
							</Tabs>
							<Box className={classes.contentChange}>
								{selectedTab === 0 && <Infoaccount infoAccount={infoAccount} />}
								{selectedTab === 1 && <Changepassword infoAccount={infoAccount} />}
								{selectedTab === 2 && <Listbookedaccount infoAccount={infoAccount} />}
							</Box>
						</Box>
					</Box>
				</Container>
			</Box>
		) : (
			<LoadComponent />
		)
	) : (
		<Redirect to="/" />
	);
};
export default withStyles(styles)(React.memo(Account));
