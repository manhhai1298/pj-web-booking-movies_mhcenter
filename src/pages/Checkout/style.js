import BackgroundMovie from '../../assets/image/background/bgListMovies.jpg';

const styles = (theme) => {
	return {
		colorCover: {},
		compCheckout: {
			color: '#fff',
			zIndex: 1,
			position: 'relative',
			backgroundImage: `url(${BackgroundMovie})`,
			backgroundSize: '100% 100%',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center',
			backgroundAttachment: 'fixed',
			backgroundColor: 'black',
			'& $colorCover': {
				position: 'absolute',
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				zIndex: -2,
				backgroundColor: 'rgb(0,0,0,0.9)',
			},
		},
		imgMovieCheckout: {
			borderRadius: 7,
			width: '7vw',
			height: '9.5vw',
		},
		gridSeats: {
			gridTemplateColumns: 'repeat(3,1fr)',
		},
		gridLeftContent: {
			paddingRight: 50,
		},
		[theme.breakpoints.down('lg')]: {
			imgMovieCheckout: {
				width: '9vw',
				height: '12vw',
			},
		},
		[theme.breakpoints.down('md')]: {
			gridLeftContent: {
				paddingRight: 40,
			},
			imgMovieCheckout: {
				width: '12vw',
				height: '16vw',
			},
		},
		[theme.breakpoints.down('sm')]: {
			imgMovieCheckout: {
				width: '15vw',
				height: '20vw',
			},
		},
		[theme.breakpoints.down('xs')]: {
			gridLeftContent: {
				paddingRight: 0,
				marginBottom: 20,
			},
			imgMovieCheckout: {
				width: '36vw',
				height: '50vw',
			},
		},
	};
};
export default styles;
