import { Box, Container, Grid, Typography, withStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import styles from './style';
import { checkoutService } from '../../Services/initServices';
import { Redirect, useRouteMatch } from 'react-router-dom';
import ListSeats from '../../components/ComponentCheckout/ListSeats/ListSeats';
import { useDispatch } from 'react-redux';
import { createAction } from '../../redux/actions/actionCreator';
import { POST_LIST_SEATS } from '../../redux/actions/type';
import ListSeatsChoiced from '../../components/ComponentCheckout/ListSeatsChoiced/ListSeatsChoiced';
import LazyLoad from 'react-lazyload';
import LoadFetch from '../../components/CompLoading/LoadComponent/LoadFetch';

const Checkout = ({ classes }) => {
	// init hook
	const route = useRouteMatch();
	const [isShowComp, setIsShowComp] = useState(false);
	const [infoMovie, setInfoMovie] = useState({});
	const dispatch = useDispatch();

	const userSignIn = JSON.parse(localStorage.getItem('infoUser'));

	//Handle after render component
	useEffect(() => {
		checkoutService
			.getInfoCheckout(route.params.scheduleId)
			.then((res) => {
				dispatch(createAction(POST_LIST_SEATS, res.data.danhSachGhe));
				setInfoMovie(res.data.thongTinPhim);
				setIsShowComp(true);
			})
			.catch();
	}, [route, dispatch]);

	return userSignIn.accessToken ? (
		isShowComp ? (
			<Box className={classes.compCheckout} py={8}>
				<Container maxWidth="lg">
					<Box className={classes.colorCover}></Box>
					<Box>
						<Grid container>
							{/* Left Content */}
							<Grid item sm={8} xs={12} className={classes.gridLeftContent}>
								<ListSeats />
							</Grid>
							{/* Right Content */}
							<Grid item sm={4} xs={12} style={{ color: 'black' }}>
								{/* Info movie */}
								<Box display="flex" justifyContent="center">
									<Box>
										<Box
											display="flex"
											style={{ backgroundColor: 'white', padding: 10, borderRadius: 7 }}
										>
											<LazyLoad once={true}>
												<img
													src={
														infoMovie.hinhAnh.indexOf('https') === -1
															? infoMovie.hinhAnh.replace('http', 'https')
															: null
													}
													alt="img-checkout-movie"
													className={classes.imgMovieCheckout}
												/>
											</LazyLoad>

											<Box ml={2}>
												<Box style={{ padding: '5px 0', borderBottom: '1px dashed #ffa300' }}>
													<Typography
														variant="h4"
														container="h4"
														style={{ color: '#ffa300' }}
													>
														{infoMovie.tenPhim}
													</Typography>
													<Typography variant="h6" container="h6">
														{infoMovie.ngayChieu} - {infoMovie.gioChieu} -{' '}
														{infoMovie.tenRap}
													</Typography>
												</Box>
												<Box style={{ padding: '5px 0' }}>
													<Typography
														variant="h5"
														container="h5"
														style={{ color: '#ffa300' }}
													>
														{infoMovie.tenCumRap}
													</Typography>
													<Typography>{infoMovie.diaChi}</Typography>
												</Box>
											</Box>
										</Box>
										<ListSeatsChoiced />
									</Box>
								</Box>
							</Grid>
						</Grid>
					</Box>
				</Container>
			</Box>
		) : (
			<LoadFetch />
		)
	) : (
		<Redirect to="/" />
	);
};
export default withStyles(styles)(React.memo(Checkout));
