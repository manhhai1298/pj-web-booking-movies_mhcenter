import React, { Component, Suspense } from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import LoadComponent from '../components/CompLoading/LoadComponent/LoadComponent';
const withLayout = (Comp, typeMenu) => {
	return class extends Component {
		render() {
			return (
				<div>
					<Header typeMenu={typeMenu} />
					<Suspense fallback={<LoadComponent />}>
						<Comp {...this.Comp} />
					</Suspense>
					<Footer />
				</div>
			);
		}
	};
};
export default withLayout;
