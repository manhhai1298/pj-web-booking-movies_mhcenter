import React, { Component } from 'react';
import { WOW } from 'wowjs';
const ListTheaterLayout = (Comp) => {
	return class extends Component {
		componentDidMount() {
			const wowListTheater = new WOW({
				boxClass: 'wowListTheater',
				live: false,
				// mobile: true,
			});
			wowListTheater.init();
		}
		render() {
			return (
				<div className="wowListTheater fadeInUp">
					<Comp {...this.Comp} />
				</div>
			);
		}
	};
};
export default ListTheaterLayout;
